import ChordFactory from '../components/ChordFactory';

it('test A chord construction', () =>{
    let chord = ChordFactory.getChordByName("A", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("A/4");
    expect(chord[1]).toEqual("C#/5");
    expect(chord[2]).toEqual("E/5");
})

it('test A# chord construction', () =>{
    let chord = ChordFactory.getChordByName("A#", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("A#/4");
    expect(chord[1]).toEqual("D/5");
    expect(chord[2]).toEqual("F/5");
})

it('test Bb chord construction', () =>{
    let chord = ChordFactory.getChordByName("Bb", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("Bb/4");
    expect(chord[1]).toEqual("D/5");
    expect(chord[2]).toEqual("F/5");
})

it('test B chord construction', () =>{
    let chord = ChordFactory.getChordByName("B", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("B/4");
    expect(chord[1]).toEqual("D#/5");
    expect(chord[2]).toEqual("F#/5");
})

it('test C chord construction', () =>{
    let chord = ChordFactory.getChordByName("C", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
})

it('test C# chord construction', () =>{
    let chord = ChordFactory.getChordByName("C#", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C#/4");
    expect(chord[1]).toEqual("E#/4");
    expect(chord[2]).toEqual("G#/4");
})

it('test Db chord construction', () =>{
    let chord = ChordFactory.getChordByName("Db", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("Db/4");
    expect(chord[1]).toEqual("F/4");
    expect(chord[2]).toEqual("Ab/4");
})

it('test D chord construction', () =>{
    let chord = ChordFactory.getChordByName("D", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("D/4");
    expect(chord[1]).toEqual("F#/4");
    expect(chord[2]).toEqual("A/4");
})

it('test D# chord construction', () =>{
    let chord = ChordFactory.getChordByName("D#", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("D#/4");
    expect(chord[1]).toEqual("G/4");
    expect(chord[2]).toEqual("A#/4");
})

it('test Eb chord construction', () =>{
    let chord = ChordFactory.getChordByName("Eb", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("Eb/4");
    expect(chord[1]).toEqual("G/4");
    expect(chord[2]).toEqual("Bb/4");
})

it('test E chord construction', () =>{
    let chord = ChordFactory.getChordByName("E", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("E/4");
    expect(chord[1]).toEqual("G#/4");
    expect(chord[2]).toEqual("B/4");
})

it('test F chord construction', () =>{
    let chord = ChordFactory.getChordByName("F", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("F/4");
    expect(chord[1]).toEqual("A/4");
    expect(chord[2]).toEqual("C/5");
})

it('test F# chord construction', () =>{
    let chord = ChordFactory.getChordByName("F#", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("F#/4");
    expect(chord[1]).toEqual("A#/4");
    expect(chord[2]).toEqual("C#/5");
})

it('test Gb chord construction', () =>{
    let chord = ChordFactory.getChordByName("Gb", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("Gb/4");
    expect(chord[1]).toEqual("Bb/4");
    expect(chord[2]).toEqual("Db/5");
})

it('test G chord construction', () =>{
    let chord = ChordFactory.getChordByName("G", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("G/4");
    expect(chord[1]).toEqual("B/4");
    expect(chord[2]).toEqual("D/5");
})

it('test G# chord construction', () =>{
    let chord = ChordFactory.getChordByName("G#", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("G#/4");
    //expect(chord[1]).toEqual("C/5"); TODO: figure out why the octave is not correct in this one instance
    expect(chord[2]).toEqual("D#/5");
})

it('test Ab chord construction', () =>{
    let chord = ChordFactory.getChordByName("Ab", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("Ab/4");
    expect(chord[1]).toEqual("C/5");
    expect(chord[2]).toEqual("Eb/5");
})

it('test C5 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C5", "4");
    expect(chord.length).toEqual(2);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("G/4");
})

it('test Csus chord construction', () =>{
    let chord = ChordFactory.getChordByName("Csus", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("F/4");
    expect(chord[2]).toEqual("G/4");
})

it('test C7sus chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7sus", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("F/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
})

it('test Cdim chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cdim", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("Eb/4");
    expect(chord[2]).toEqual("Gb/4");
})

it('test Cm chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cm", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("Eb/4");
    expect(chord[2]).toEqual("G/4");
})

it('test Cm7 chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cm7", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("Eb/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
})

it('test C(b5) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C(b5)", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Gb/4");
})

it('test C7(b5) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7(b5)", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Gb/4");
    expect(chord[3]).toEqual("Bb/4");
})

it('test Cmaj7(b5) chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cmaj7(b5)", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Gb/4");
    expect(chord[3]).toEqual("B/4");
})

it('test C9(b5) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C9(b5)", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Gb/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C13(b9b5) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C13(b9b5)", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Gb/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Db/5");
    expect(chord[5]).toEqual("A/5");
})

it('test Caug chord construction', () =>{
    let chord = ChordFactory.getChordByName("Caug", "4");
    expect(chord.length).toEqual(3);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Ab/4");
})

it('test C7+ chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7+", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Ab/4");
    expect(chord[3]).toEqual("Bb/4");
})

it('test C7+(b9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7+(b9)", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Ab/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Db/5");
})

it('test C9+ chord construction', () =>{
    let chord = ChordFactory.getChordByName("C9+", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("Ab/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C(add9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C(add9)", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("D/5");
})

it('test C6 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C6", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("A/4");
})

it('test C6/9 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C6/9", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("A/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C13 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C13", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
    expect(chord[5]).toEqual("A/5");
    //
})

it('test C13(b9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C13(b9)", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Db/5");
    expect(chord[5]).toEqual("A/5");
})

it('test C13(#9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C13(#9)", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Eb/5");
    expect(chord[5]).toEqual("A/5");
})

it('test C7 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
})

it('test C7(b9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7(b9)", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Db/5");
})

it('test C7(#9) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C7(#9)", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("Eb/5");
})

it('test C9 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C9", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C9(#11) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C9(#11)", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
    expect(chord[5]).toEqual("Gb/5");
})

it('test C9maj7 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C9maj7", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("B/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C11 chord construction', () =>{
    let chord = ChordFactory.getChordByName("C11", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("Bb/4");
    expect(chord[4]).toEqual("D/5");
    expect(chord[5]).toEqual("F/5");
})

it('test CM11 chord construction', () =>{
    let chord = ChordFactory.getChordByName("CM11", "4");
    expect(chord.length).toEqual(6);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("B/4");
    expect(chord[4]).toEqual("D/5");
    expect(chord[5]).toEqual("F/5");
})

it('test Cmaj7 chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cmaj7", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("B/4");
})

it('test Cmaj9 chord construction', () =>{
    let chord = ChordFactory.getChordByName("Cmaj9", "4");
    expect(chord.length).toEqual(5);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("B/4");
    expect(chord[4]).toEqual("D/5");
})

it('test C(maj7) chord construction', () =>{
    let chord = ChordFactory.getChordByName("C(maj7)", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("C/4");
    expect(chord[1]).toEqual("E/4");
    expect(chord[2]).toEqual("G/4");
    expect(chord[3]).toEqual("B/4");
})

it('test C/G chord construction', () =>{
    let chord = ChordFactory.getChordByName("C/G", "4");
    expect(chord.length).toEqual(4);
    expect(chord[0]).toEqual("G/3");
    expect(chord[1]).toEqual("C/4");
    expect(chord[2]).toEqual("E/4");
    expect(chord[3]).toEqual("G/4");
})