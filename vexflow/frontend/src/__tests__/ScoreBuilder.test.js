import React from 'react'
import ReactDOM from 'react-dom'
import ScoreBuilder from '../components/scorebuilder/ScoreBuilder'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'

const wrapper = mount(<ScoreBuilder />);
const NoteUtils = require('../components/scorebuilder/utils/NoteUtils')

it('should set score to helloWorld, sets tool to ', () => {
    const button = wrapper.find('#btn1').at(0)
    button.simulate('click');
    const score = wrapper.find('#scoreObject').at(0)
    expect(wrapper.instance().state.score).toBe("helloWorld")
    const toolButton = wrapper.find('#tool-selector-tab-2').at(0)
    toolButton.simulate("click")
    expect(wrapper.instance().tool.current.currentTool).toBe("2")
    expect(wrapper.instance().tool.current.getSubTool()).toBe(0)
});

describe('Test ScoreBuilder Tools', () => {
    let someNote

    beforeEach(() => {
        wrapper.find('#btn1').at(0).simulate('click');
        someNote = wrapper.find('#scoreObject').at(0).instance().parts[0].measures[0].notes[0]
        someNote.vexNote.calculateKeyProps = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.buildStem = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.reset = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.buildFlag = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.setStave = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.autoStem = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.draw = jest.fn().mockImplementation(() => { return null })
        someNote.vexNote.getAttribute = jest.fn().mockImplementation(() => { return fakeAttribute })
        someNote.context.svg.appendChild = jest.fn().mockImplementation(() => { return null });
        someNote.score.tool.current.getSubTool = jest.fn().mockImplementation(() => { return 1 })
    })

    it('uses pitchtool to mousemove', () => {
        someNote.context.svg.createSVGPoint = jest.fn().mockImplementation(() => {  return fakeSVGPoint;  });
        fakeSVGPoint.matrixTransform = jest.fn().mockImplementation(() => {return {y:0}})
        someNote.context.svg.getScreenCTM = jest.fn().mockImplementation(() => { return fakeCTMReturn })
        fakeCTMReturn.inverse = jest.fn().mockImplementation(() => { return null })

        NoteUtils.pitchTool(fakeMouseEvent("mousemove"), someNote)
    })

    it('uses pitchtool to click', () => {
        fakeAttribute.remove = jest.fn().mockImplementation(() => { return null })
        someNote.pitch = "c5"
        NoteUtils.pitchTool(fakeMouseEvent("click"), someNote)
    })

    it('uses pitchtool to mouseout', () => {
        NoteUtils.pitchTool(fakeMouseEvent("mouseout"), someNote)
    })

    it('uses durationTool to click', () => {
        NoteUtils.durationTool(fakeMouseEvent("click"), someNote)
    })

    it('uses durationTool to mouseout', () => {
        someNote.temp_props.duration = "w"
        NoteUtils.durationTool(fakeMouseEvent("mouseout"), someNote)
    })

    it('uses durationTool to mouseenter', () => {
        NoteUtils.durationTool(fakeMouseEvent("mouseenter"), someNote)
    })

    it('uses accidentalTool to click', () => {
        NoteUtils.accidentalTool(fakeMouseEvent("click"), someNote)
    })

    it('uses accidentalTool to doubleclick', () => {
        NoteUtils.accidentalTool(fakeMouseEvent("dblclick"), someNote)
    })

    it('uses accidentalTool to mouseout', () => {
        someNote.temp_props.modifiers = "string";
        NoteUtils.accidentalTool(fakeMouseEvent("mouseout"), someNote)
    })

    it('uses accidentalTool to mouseenter', () => {
        NoteUtils.accidentalTool(fakeMouseEvent("mouseenter"), someNote)
    })

    it('hits all default cases for tools', () => {
        NoteUtils.accidentalTool(fakeMouseEvent("fake"), someNote)
        NoteUtils.durationTool(fakeMouseEvent("fake"), someNote)
        NoteUtils.pitchTool(fakeMouseEvent("fake"), someNote)
    })
    
    it('fakes all mouse events', () => {
        someNote.createEventListeners()

        someNote.score.tool.current.currentTool = "1"
        map.click("click")
        map.dblclick("dblclick")
        map.mouseenter("mouseenter")
        map.mouseout("mouseout")
        map.mousemove("mousemove")

        someNote.score.tool.current.currentTool = "2"
        map.click("click")
        map.dblclick("dblclick")
        map.mouseenter("mouseenter")
        map.mouseout("mouseout")
        map.mousemove("mousemove")

        someNote.score.tool.current.currentTool = "3"
        map.click("click")
        map.dblclick("dblclick")
        map.mouseenter("mouseenter")
        map.mouseout("mouseout")
        map.mousemove("mousemove")
    })
})

function fakeMouseEvent(type) {
    return {
        type: type,
        clientX: 10,
        clientY: 10,
        target : {
            attributes :{
                y :{
                    value: 10
                }
            }
        }
    }
}

const fakeSVGPoint = {
    x: null,
    y: null,
}

const fakeCTMReturn = {}

var map = {};

const fakeAttribute = {
    childNodes: [
        {
            x: {baseVal : {value : 0}},
            width: {baseVal : {value : 0}}
        }
    ],
    addEventListener: jest.fn((event, callback, boolean) => {
        map[event] = callback;
    }),
    remove: function (){}
}
