import reducer from "../store/reducers/auth"

test('authStart', () => {
    let state;
    state = reducer({token:null,error:null,loading:false}, {type:'AUTH_START'});
    expect(state).toEqual({token:null,error:null,loading:true});
});

test('authSuccess', () => {
  let state;
  state = reducer({token:null,error:null,loading:true}, {type:'AUTH_SUCCESS',token:'d6beed2a7ed037521a4ebfaf41cab627a7588a87'});
  expect(state).toEqual({token:'d6beed2a7ed037521a4ebfaf41cab627a7588a87',error:null,loading:false});
});

test('authFail', () => {
    let state;
    state = reducer({token:null,error:null,loading:true}, {type:'AUTH_FAIL',error:{config:{transformRequest:{},transformResponse:{},timeout:0,xsrfCookieName:'XSRF-TOKEN',xsrfHeaderName:'X-XSRF-TOKEN',maxContentLength:-1,headers:{Accept:'application/json, text/plain, */*','Content-Type':'application/json;charset=utf-8'},method:'post',url:'http://127.0.0.1:8000/rest-auth/login/',data:'{"username":"bad","password":"bad"}'},request:{},response:{data:{non_field_errors:['Unable to log in with provided credentials.']},status:400,statusText:'Bad Request',headers:{'content-type':'application/json'},config:{transformRequest:{},transformResponse:{},timeout:0,xsrfCookieName:'XSRF-TOKEN',xsrfHeaderName:'X-XSRF-TOKEN',maxContentLength:-1,headers:{Accept:'application/json, text/plain, */*','Content-Type':'application/json;charset=utf-8'},method:'post',url:'http://127.0.0.1:8000/rest-auth/login/',data:'{"username":"bad","password":"bad"}'},request:{}}}});
    expect(state).toEqual({token:null,error:{config:{transformRequest:{},transformResponse:{},timeout:0,xsrfCookieName:'XSRF-TOKEN',xsrfHeaderName:'X-XSRF-TOKEN',maxContentLength:-1,headers:{Accept:'application/json, text/plain, */*','Content-Type':'application/json;charset=utf-8'},method:'post',url:'http://127.0.0.1:8000/rest-auth/login/',data:'{"username":"bad","password":"bad"}'},request:{},response:{data:{non_field_errors:['Unable to log in with provided credentials.']},status:400,statusText:'Bad Request',headers:{'content-type':'application/json'},config:{transformRequest:{},transformResponse:{},timeout:0,xsrfCookieName:'XSRF-TOKEN',xsrfHeaderName:'X-XSRF-TOKEN',maxContentLength:-1,headers:{Accept:'application/json, text/plain, */*','Content-Type':'application/json;charset=utf-8'},method:'post',url:'http://127.0.0.1:8000/rest-auth/login/',data:'{"username":"bad","password":"bad"}'},request:{}}},loading:false});
});

test('authLogout', () => {
    let state;
    state = reducer({token:'d6beed2a7ed037521a4ebfaf41cab627a7588a87',error:null,loading:false}, {type:'AUTH_LOGOUT'});
    expect(state).toEqual({token:null,error:null,loading:false});
});