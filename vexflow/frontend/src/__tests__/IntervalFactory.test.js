import IntervalFactory from '../components/IntervalFactory';

it('test getIntervalByName R', () => {
    let interval = IntervalFactory.getIntervalByName("R");
    expect(interval.getIntervalValue()).toEqual(0);
})

it('test getIntervalByName m2', () => {
    let interval = IntervalFactory.getIntervalByName("m2");
    expect(interval.getIntervalValue()).toEqual(1);
})

it('test getIntervalByName M2', () => {
    let interval = IntervalFactory.getIntervalByName("M2");
    expect(interval.getIntervalValue()).toEqual(2);
})

it('test getIntervalByName dim3', () => {
    let interval = IntervalFactory.getIntervalByName("dim3");
    expect(interval.getIntervalValue()).toEqual(2);
})

it('test getIntervalByName m3', () => {
    let interval = IntervalFactory.getIntervalByName("m3");
    expect(interval.getIntervalValue()).toEqual(3);
})

it('test getIntervalByName aug2', () => {
    let interval = IntervalFactory.getIntervalByName("aug2");
    expect(interval.getIntervalValue()).toEqual(3);
})

it('test getIntervalByName M3', () => {
    let interval = IntervalFactory.getIntervalByName("M3");
    expect(interval.getIntervalValue()).toEqual(4);
})

it('test getIntervalByName dim4', () => {
    let interval = IntervalFactory.getIntervalByName("dim4");
    expect(interval.getIntervalValue()).toEqual(4);
})

it('test getIntervalByName P4', () => {
    let interval = IntervalFactory.getIntervalByName("P4");
    expect(interval.getIntervalValue()).toEqual(5);
})

it('test getIntervalByName aug3', () => {
    let interval = IntervalFactory.getIntervalByName("aug3");
    expect(interval.getIntervalValue()).toEqual(5);
})

it('test getIntervalByName aug4', () => {
    let interval = IntervalFactory.getIntervalByName("aug4");
    expect(interval.getIntervalValue()).toEqual(6);
})

it('test getIntervalByName dim5', () => {
    let interval = IntervalFactory.getIntervalByName("dim5");
    expect(interval.getIntervalValue()).toEqual(6);
})

it('test getIntervalByName P5', () => {
    let interval = IntervalFactory.getIntervalByName("P5");
    expect(interval.getIntervalValue()).toEqual(7);
})

it('test getIntervalByName dim6', () => {
    let interval = IntervalFactory.getIntervalByName("dim6");
    expect(interval.getIntervalValue()).toEqual(7);
})

it('test getIntervalByName m6', () => {
    let interval = IntervalFactory.getIntervalByName("m6");
    expect(interval.getIntervalValue()).toEqual(8);
})

it('test getIntervalByName aug5', () => {
    let interval = IntervalFactory.getIntervalByName("aug5");
    expect(interval.getIntervalValue()).toEqual(8);
})

it('test getIntervalByName M6', () => {
    let interval = IntervalFactory.getIntervalByName("M6");
    expect(interval.getIntervalValue()).toEqual(9);
})

it('test getIntervalByName dim7', () => {
    let interval = IntervalFactory.getIntervalByName("dim7");
    expect(interval.getIntervalValue()).toEqual(9);
})

it('test getIntervalByName m7', () => {
    let interval = IntervalFactory.getIntervalByName("m7");
    expect(interval.getIntervalValue()).toEqual(10);
})

it('test getIntervalByName aug6', () => {
    let interval = IntervalFactory.getIntervalByName("aug6");
    expect(interval.getIntervalValue()).toEqual(10);
})

it('test getIntervalByName M7', () => {
    let interval = IntervalFactory.getIntervalByName("M7");
    expect(interval.getIntervalValue()).toEqual(11);
})

it('test getIntervalByName R', () => {
    let interval = IntervalFactory.getIntervalByName("R");
    expect(interval.getIntervalValue()).toEqual(0);
})

it('test getIntervalByValue 0', () => {
    let interval = IntervalFactory.getIntervalByValue(0);
    expect(interval.getName()).toEqual("R");
})

it('test getIntervalByValue 1', () => {
    let interval = IntervalFactory.getIntervalByValue(1);
    expect(interval.getName()).toEqual("m2");
})

it('test getIntervalByValue 2', () => {
    let interval = IntervalFactory.getIntervalByValue(2);
    expect(interval.getName()).toEqual("M2");
})

it('test getIntervalByValue 3', () => {
    let interval = IntervalFactory.getIntervalByValue(3);
    expect(interval.getName()).toEqual("m3");
})

it('test getIntervalByValue 4', () => {
    let interval = IntervalFactory.getIntervalByValue(4);
    expect(interval.getName()).toEqual("M3");
})

it('test getIntervalByValue 5', () => {
    let interval = IntervalFactory.getIntervalByValue(5);
    expect(interval.getName()).toEqual("P4");
})

it('test getIntervalByValue 6', () => {
    let interval = IntervalFactory.getIntervalByValue(6);
    expect(interval.getName()).toEqual("dim5");
})

it('test getIntervalByValue 7', () => {
    let interval = IntervalFactory.getIntervalByValue(7);
    expect(interval.getName()).toEqual("P5");
})

it('test getIntervalByValue 8', () => {
    let interval = IntervalFactory.getIntervalByValue(8);
    expect(interval.getName()).toEqual("m6");
})

it('test getIntervalByValue 9', () => {
    let interval = IntervalFactory.getIntervalByValue(9);
    expect(interval.getName()).toEqual("M6");
})

it('test getIntervalByValue 10', () => {
    let interval = IntervalFactory.getIntervalByValue(10);
    expect(interval.getName()).toEqual("m7");
})

it('test getIntervalByValue 11', () => {
    let interval = IntervalFactory.getIntervalByValue(11);
    expect(interval.getName()).toEqual("M7");
})