import NoteFactory from '../components/NoteFactory';

it('test calculate A', () => {
    let note = NoteFactory.getNoteByValue(0,"A");
    expect(note).toEqual("A");
})

it('test calculate Bb', () => {
    let note = NoteFactory.getNoteByValue(1,"A");
    expect(note).toEqual("Bb");
})

it('test calculate B', () => {
    let note = NoteFactory.getNoteByValue(2,"A");
    expect(note).toEqual("B");
})

it('test calculate C', () => {
    let note = NoteFactory.getNoteByValue(3,"A");
    expect(note).toEqual("C");
})

it('test calculate C#', () => {
    let note = NoteFactory.getNoteByValue(4,"A");
    expect(note).toEqual("C#");
})

it('test calculate D', () => {
    let note = NoteFactory.getNoteByValue(5,"A");
    expect(note).toEqual("D");
})

it('test calculate Eb', () => {
    let note = NoteFactory.getNoteByValue(6,"A");
    expect(note).toEqual("Eb");
})

it('test calculate E', () => {
    let note = NoteFactory.getNoteByValue(7,"A");
    expect(note).toEqual("E");
})

it('test calculate F', () => {
    let note = NoteFactory.getNoteByValue(8,"A");
    expect(note).toEqual("F");
})

it('test calculate F#', () => {
    let note = NoteFactory.getNoteByValue(9,"A");
    expect(note).toEqual("F#");
})

it('test calculate G', () => {
    let note = NoteFactory.getNoteByValue(10,"A");
    expect(note).toEqual("G");
})

it('test calculate G#', () => {
    let note = NoteFactory.getNoteByValue(11,"A");
    expect(note).toEqual("G#");
})

it('test calculate getNoteValue', () => 
{
    let note;
    note = "A";
    expect(NoteFactory.getNoteValue(note)).toEqual(1);
    note = "A#";
    expect(NoteFactory.getNoteValue(note)).toEqual(2);
    note = "Bb";
    expect(NoteFactory.getNoteValue(note)).toEqual(2);
    note = "B";
    expect(NoteFactory.getNoteValue(note)).toEqual(3);
    note = "C";
    expect(NoteFactory.getNoteValue(note)).toEqual(4);
    note = "C#";
    expect(NoteFactory.getNoteValue(note)).toEqual(5);
    note = "Db";
    expect(NoteFactory.getNoteValue(note)).toEqual(5);
    note = "D";
    expect(NoteFactory.getNoteValue(note)).toEqual(6);
    note = "D#";
    expect(NoteFactory.getNoteValue(note)).toEqual(7);
    note = "Eb";
    expect(NoteFactory.getNoteValue(note)).toEqual(7);
    note = "E";
    expect(NoteFactory.getNoteValue(note)).toEqual(8);
    note = "F";
    expect(NoteFactory.getNoteValue(note)).toEqual(9);
    note = "F#";
    expect(NoteFactory.getNoteValue(note)).toEqual(10);
    note = "Gb";
    expect(NoteFactory.getNoteValue(note)).toEqual(10);
    note = "G";
    expect(NoteFactory.getNoteValue(note)).toEqual(11);
    note = "G#";
    expect(NoteFactory.getNoteValue(note)).toEqual(12);
    note = "Ab";
    expect(NoteFactory.getNoteValue(note)).toEqual(12);
})

it('test getNoteByValue', () => 
{
    let note;
    let key = "A";
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("G#");

    key = "A#";
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("G#");

    key = "Bb";
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Cb");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("Ab");

    key = "B";
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("G#");

    key = "C";
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("Ab");

    key = "C#";
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("B#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("E#");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("G#");

    key = "Db";
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("Ab");

    key = "D";
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("Ab");

    key = "D#";
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("G#");

    key = "Eb";
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Fb");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Ab");

    key = "E";
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("G#");

    key = "F";
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("Ab");

    key = "F#";
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("E#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("G#");

    key = "Gb";
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Cb");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("Ab");

    key = "G";
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("Ab");

    key = "G#";
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("A#");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("C#");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("D#");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("F#");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("G#");

    key = "Ab";
    note = NoteFactory.getNoteByValue(1, key);
    expect(note).toEqual("A");
    note = NoteFactory.getNoteByValue(2, key);
    expect(note).toEqual("Bb");
    note = NoteFactory.getNoteByValue(3, key);
    expect(note).toEqual("B");
    note = NoteFactory.getNoteByValue(4, key);
    expect(note).toEqual("C");
    note = NoteFactory.getNoteByValue(5, key);
    expect(note).toEqual("Db");
    note = NoteFactory.getNoteByValue(6, key);
    expect(note).toEqual("D");
    note = NoteFactory.getNoteByValue(7, key);
    expect(note).toEqual("Eb");
    note = NoteFactory.getNoteByValue(8, key);
    expect(note).toEqual("E");
    note = NoteFactory.getNoteByValue(9, key);
    expect(note).toEqual("F");
    note = NoteFactory.getNoteByValue(10, key);
    expect(note).toEqual("Gb");
    note = NoteFactory.getNoteByValue(11, key);
    expect(note).toEqual("G");
    note = NoteFactory.getNoteByValue(0, key);
    expect(note).toEqual("Ab");
})