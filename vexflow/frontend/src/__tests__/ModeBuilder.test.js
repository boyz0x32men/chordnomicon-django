import React from 'react';
import ReactDOM from 'react-dom';
import ModeBuilder from '../components/modebuilder/ModeBuilder';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ModeBuilder />, div);
    ReactDOM.unmountComponentAtNode(div);
});