import ModeFactory from '../components/ModeFactory';
import RecommendedChordFactory from '../components/ChordRecommendationFactory';

it('test first recommended degrees on C Major', () => {
    let recommendedDegrees = RecommendedChordFactory.getFirstRecommendedDegrees("C", ModeFactory.getModeByName("Major"));
    expect(recommendedDegrees[0]).toEqual("C");
    expect(recommendedDegrees[1]).toEqual("G");
    expect(recommendedDegrees[2]).toEqual("F");
    expect(recommendedDegrees[3]).toEqual("A");
    expect(recommendedDegrees[4]).toEqual("E");
    expect(recommendedDegrees[5]).toEqual("D");
    expect(recommendedDegrees[6]).toEqual("B");
})

it('test first recommended degrees on C Locrian', () => {
    let recommendedDegrees = RecommendedChordFactory.getFirstRecommendedDegrees("C", ModeFactory.getModeByName("Locrian"));
    expect(recommendedDegrees[0]).toEqual("C");
    expect(recommendedDegrees[1]).toEqual("F");
    expect(recommendedDegrees[2]).toEqual("Ab");
    expect(recommendedDegrees[3]).toEqual("Eb");
    expect(recommendedDegrees[4]).toEqual("Db");
    expect(recommendedDegrees[5]).toEqual("Bb");
    expect(recommendedDegrees[6]).toEqual("Gb");
})

it('test get recommended degrees by last C in C Major', () => {
    let key = "C";
    let lastTonic = "C";
    let mode = ModeFactory.getModeByName("Major");
    let recommendedDegrees = RecommendedChordFactory.getRecommendedDegreesByLast(key, lastTonic, mode);
    expect(recommendedDegrees[0]).toEqual("F");
    expect(recommendedDegrees[1]).toEqual("D");
    expect(recommendedDegrees[2]).toEqual("E");
    expect(recommendedDegrees[3]).toEqual("G");
    expect(recommendedDegrees[4]).toEqual("B");
    expect(recommendedDegrees[5]).toEqual("A");
    expect(recommendedDegrees[6]).toEqual("C");
})

it('test get recommended degrees by last C in C Locrian', () => {
    let key = "C";
    let lastTonic = "C";
    let mode = ModeFactory.getModeByName("Locrian");
    let recommendedDegrees = RecommendedChordFactory.getRecommendedDegreesByLast(key, lastTonic, mode);
    expect(recommendedDegrees[0]).toEqual("F");
    expect(recommendedDegrees[1]).toEqual("Db");
    expect(recommendedDegrees[2]).toEqual("Eb");
    expect(recommendedDegrees[3]).toEqual("Bb");
    expect(recommendedDegrees[4]).toEqual("Ab");
    expect(recommendedDegrees[5]).toEqual("Gb");
    expect(recommendedDegrees[6]).toEqual("C");
})

it('test get recommended degrees by last B in C Major', () => {
    let key = "C";
    let lastTonic = "B";
    let mode = ModeFactory.getModeByName("Major");
    let recommendedDegrees = RecommendedChordFactory.getRecommendedDegreesByLast(key, lastTonic, mode);
    expect(recommendedDegrees[0]).toEqual("E");
    expect(recommendedDegrees[1]).toEqual("C");
    expect(recommendedDegrees[2]).toEqual("D");
    expect(recommendedDegrees[3]).toEqual("A");
    expect(recommendedDegrees[4]).toEqual("G");
    expect(recommendedDegrees[5]).toEqual("F");
    expect(recommendedDegrees[6]).toEqual("B");
})

it('test get chords by degree, C in C Major', () => {
    let key = "C";
    let tonic = "C";
    let duration = "3"
    let mode = ModeFactory.getModeByName("Major");
    let recommendedChords = RecommendedChordFactory.getChordsByDegree(key, tonic, mode, duration);
    expect(recommendedChords.length).toEqual(10);
    // C
    expect(recommendedChords[0].length).toEqual(3);
    expect(recommendedChords[0][0]).toEqual("C/3");
    expect(recommendedChords[0][1]).toEqual("E/3");
    expect(recommendedChords[0][2]).toEqual("G/3");
    // C5
    expect(recommendedChords[1].length).toEqual(2);
    expect(recommendedChords[1][0]).toEqual("C/3");
    expect(recommendedChords[1][1]).toEqual("G/3");
    // Csus
    expect(recommendedChords[2].length).toEqual(3);
    expect(recommendedChords[2][0]).toEqual("C/3");
    expect(recommendedChords[2][1]).toEqual("F/3");
    expect(recommendedChords[2][2]).toEqual("G/3");
    // Csus2
    expect(recommendedChords[3].length).toEqual(3);
    expect(recommendedChords[3][0]).toEqual("C/3");
    expect(recommendedChords[3][1]).toEqual("D/3");
    expect(recommendedChords[3][2]).toEqual("G/3");
    // Cmaj7
    expect(recommendedChords[4].length).toEqual(4);
    expect(recommendedChords[4][0]).toEqual("C/3");
    expect(recommendedChords[4][1]).toEqual("E/3");
    expect(recommendedChords[4][2]).toEqual("G/3");
    expect(recommendedChords[4][3]).toEqual("B/3");
    // C6
    expect(recommendedChords[5].length).toEqual(4);
    expect(recommendedChords[5][0]).toEqual("C/3");
    expect(recommendedChords[5][1]).toEqual("E/3");
    expect(recommendedChords[5][2]).toEqual("G/3");
    expect(recommendedChords[5][3]).toEqual("A/3");
    // D6/9
    expect(recommendedChords[6].length).toEqual(5);
    expect(recommendedChords[6][0]).toEqual("C/3");
    expect(recommendedChords[6][1]).toEqual("E/3");
    expect(recommendedChords[6][2]).toEqual("G/3");
    expect(recommendedChords[6][3]).toEqual("A/3");
    expect(recommendedChords[6][4]).toEqual("D/4");
    // C(add9)
    expect(recommendedChords[7].length).toEqual(4);
    expect(recommendedChords[7][0]).toEqual("C/3");
    expect(recommendedChords[7][1]).toEqual("E/3");
    expect(recommendedChords[7][2]).toEqual("G/3");
    expect(recommendedChords[7][3]).toEqual("D/4");
    // Cmaj9
    expect(recommendedChords[8].length).toEqual(5);
    expect(recommendedChords[8][0]).toEqual("C/3");
    expect(recommendedChords[8][1]).toEqual("E/3");
    expect(recommendedChords[8][2]).toEqual("G/3");
    expect(recommendedChords[8][3]).toEqual("B/3");
    expect(recommendedChords[8][4]).toEqual("D/4");
    // CM11
    expect(recommendedChords[9].length).toEqual(6);
    expect(recommendedChords[9][0]).toEqual("C/3");
    expect(recommendedChords[9][1]).toEqual("E/3");
    expect(recommendedChords[9][2]).toEqual("G/3");
    expect(recommendedChords[9][3]).toEqual("B/3");
    expect(recommendedChords[9][4]).toEqual("D/4");
    expect(recommendedChords[9][5]).toEqual("F/4");
})

it('test get chords by degree, D in C Major', () => {
    let key = "C";
    let tonic = "D";
    let duration = "3"
    let mode = ModeFactory.getModeByName("Major");
    let recommendedChords = RecommendedChordFactory.getChordsByDegree(key, tonic, mode, duration);
    expect(recommendedChords.length).toEqual(9);
    // Dm
    expect(recommendedChords[0].length).toEqual(3);
    expect(recommendedChords[0][0]).toEqual("D/3");
    expect(recommendedChords[0][1]).toEqual("F/3");
    expect(recommendedChords[0][2]).toEqual("A/3");
    // D5
    expect(recommendedChords[1].length).toEqual(2);
    expect(recommendedChords[1][0]).toEqual("D/3");
    expect(recommendedChords[1][1]).toEqual("A/3");
    // Dsus
    expect(recommendedChords[2].length).toEqual(3);
    expect(recommendedChords[2][0]).toEqual("D/3");
    expect(recommendedChords[2][1]).toEqual("G/3");
    expect(recommendedChords[2][2]).toEqual("A/3");
    // Dsus2
    expect(recommendedChords[3].length).toEqual(3);
    expect(recommendedChords[3][0]).toEqual("D/3");
    expect(recommendedChords[3][1]).toEqual("E/3");
    expect(recommendedChords[3][2]).toEqual("A/3");
    // Dm7
    expect(recommendedChords[4].length).toEqual(4);
    expect(recommendedChords[4][0]).toEqual("D/3");
    expect(recommendedChords[4][1]).toEqual("F/3");
    expect(recommendedChords[4][2]).toEqual("A/3");
    expect(recommendedChords[4][3]).toEqual("C/4");
    // D7sus
    expect(recommendedChords[5].length).toEqual(4);
    expect(recommendedChords[5][0]).toEqual("D/3");
    expect(recommendedChords[5][1]).toEqual("G/3");
    expect(recommendedChords[5][2]).toEqual("A/3");
    expect(recommendedChords[5][3]).toEqual("C/4");
    // Dm6
    expect(recommendedChords[6].length).toEqual(4);
    expect(recommendedChords[6][0]).toEqual("D/3");
    expect(recommendedChords[6][1]).toEqual("F/3");
    expect(recommendedChords[6][2]).toEqual("A/3");
    expect(recommendedChords[6][3]).toEqual("B/3");
    // Dm6/9
    expect(recommendedChords[7].length).toEqual(5);
    expect(recommendedChords[7][0]).toEqual("D/3");
    expect(recommendedChords[7][1]).toEqual("F/3");
    expect(recommendedChords[7][2]).toEqual("A/3");
    expect(recommendedChords[7][3]).toEqual("B/3");
    expect(recommendedChords[7][4]).toEqual("E/4");
    // Dm11
    expect(recommendedChords[8].length).toEqual(6);
    expect(recommendedChords[8][0]).toEqual("D/3");
    expect(recommendedChords[8][1]).toEqual("F/3");
    expect(recommendedChords[8][2]).toEqual("A/3");
    expect(recommendedChords[8][3]).toEqual("C/4");
    expect(recommendedChords[8][4]).toEqual("E/4");
    expect(recommendedChords[8][5]).toEqual("G/4");
})

it('test get chords by degree, E in E Phrygian', () => {
    let key = "E";
    let tonic = "E";
    let duration = "3"
    let mode = ModeFactory.getModeByName("Phrygian");
    let recommendedChords = RecommendedChordFactory.getChordsByDegree(key, tonic, mode, duration);
    expect(recommendedChords.length).toEqual(5);
    // Em
    expect(recommendedChords[0].length).toEqual(3);
    expect(recommendedChords[0][0]).toEqual("E/3");
    expect(recommendedChords[0][1]).toEqual("G/3");
    expect(recommendedChords[0][2]).toEqual("B/3");
    // E5
    expect(recommendedChords[1].length).toEqual(2);
    expect(recommendedChords[1][0]).toEqual("E/3");
    expect(recommendedChords[1][1]).toEqual("B/3");
    // Esus
    expect(recommendedChords[2].length).toEqual(3);
    expect(recommendedChords[2][0]).toEqual("E/3");
    expect(recommendedChords[2][1]).toEqual("A/3");
    expect(recommendedChords[2][2]).toEqual("B/3");
    // E7
    expect(recommendedChords[3].length).toEqual(4);
    expect(recommendedChords[3][0]).toEqual("E/3");
    expect(recommendedChords[3][1]).toEqual("G/3");
    expect(recommendedChords[3][2]).toEqual("B/3");
    expect(recommendedChords[3][3]).toEqual("D/4");
    // Esus7
    expect(recommendedChords[4].length).toEqual(4);
    expect(recommendedChords[4][0]).toEqual("E/3");
    expect(recommendedChords[4][1]).toEqual("A/3");
    expect(recommendedChords[4][2]).toEqual("B/3");
    expect(recommendedChords[4][3]).toEqual("D/4");
})

it('test get chords by degree, F in F Lydian', () => {
    let key = "F";
    let tonic = "F";
    let duration = "3"
    let mode = ModeFactory.getModeByName("Lydian");
    let recommendedChords = RecommendedChordFactory.getChordsByDegree(key, tonic, mode, duration);
    expect(recommendedChords.length).toEqual(10);
    // Fm
    expect(recommendedChords[0].length).toEqual(3);
    expect(recommendedChords[0][0]).toEqual("F/3");
    expect(recommendedChords[0][1]).toEqual("A/3");
    expect(recommendedChords[0][2]).toEqual("C/4");
    // Fdim
    expect(recommendedChords[1].length).toEqual(3);
    expect(recommendedChords[1][0]).toEqual("F/3");
    expect(recommendedChords[1][1]).toEqual("A/3");
    expect(recommendedChords[1][2]).toEqual("B/3");
    // F5
    expect(recommendedChords[2].length).toEqual(2);
    expect(recommendedChords[2][0]).toEqual("F/3");
    expect(recommendedChords[2][1]).toEqual("C/4");
    // Fsus2
    expect(recommendedChords[3].length).toEqual(3);
    expect(recommendedChords[3][0]).toEqual("F/3");
    expect(recommendedChords[3][1]).toEqual("G/3");
    expect(recommendedChords[3][2]).toEqual("C/4");
    // Fmaj7
    expect(recommendedChords[4].length).toEqual(4);
    expect(recommendedChords[4][0]).toEqual("F/3");
    expect(recommendedChords[4][1]).toEqual("A/3");
    expect(recommendedChords[4][2]).toEqual("C/4");
    expect(recommendedChords[4][3]).toEqual("E/4");
    // F7(b5)
    expect(recommendedChords[5].length).toEqual(4);
    expect(recommendedChords[5][0]).toEqual("F/3");
    expect(recommendedChords[5][1]).toEqual("A/3");
    expect(recommendedChords[5][2]).toEqual("B/3");
    expect(recommendedChords[5][3]).toEqual("E/4");
    // F6
    expect(recommendedChords[6].length).toEqual(4);
    expect(recommendedChords[6][0]).toEqual("F/3");
    expect(recommendedChords[6][1]).toEqual("A/3");
    expect(recommendedChords[6][2]).toEqual("C/4");
    expect(recommendedChords[6][3]).toEqual("D/4");
    // F6/9
    expect(recommendedChords[7].length).toEqual(5);
    expect(recommendedChords[7][0]).toEqual("F/3");
    expect(recommendedChords[7][1]).toEqual("A/3");
    expect(recommendedChords[7][2]).toEqual("C/4");
    expect(recommendedChords[7][3]).toEqual("D/4");
    expect(recommendedChords[7][4]).toEqual("G/4");
    // F(add9)
    expect(recommendedChords[8].length).toEqual(4);
    expect(recommendedChords[8][0]).toEqual("F/3");
    expect(recommendedChords[8][1]).toEqual("A/3");
    expect(recommendedChords[8][2]).toEqual("C/4");
    expect(recommendedChords[8][3]).toEqual("G/4");
    // Fmaj9
    expect(recommendedChords[9].length).toEqual(5);
    expect(recommendedChords[9][0]).toEqual("F/3");
    expect(recommendedChords[9][1]).toEqual("A/3");
    expect(recommendedChords[9][2]).toEqual("C/4");
    expect(recommendedChords[9][3]).toEqual("E/4");
    expect(recommendedChords[9][4]).toEqual("G/4");
})

it('test get chords by degree, A in A Chromatic', () => {
    let key = "A";
    let tonic = "A";
    let duration = "3"
    let mode = ModeFactory.getModeByName("Chromatic");
    let recommendedChords = RecommendedChordFactory.getChordsByDegree(key, tonic, mode, duration);
    expect(recommendedChords.length).toEqual(38);
    // A
    expect(recommendedChords[0].length).toEqual(3);
    expect(recommendedChords[0][0]).toEqual("A/3");
    expect(recommendedChords[0][1]).toEqual("C#/4");
    expect(recommendedChords[0][2]).toEqual("E/4");
    // Am
    expect(recommendedChords[1].length).toEqual(3);
    expect(recommendedChords[1][0]).toEqual("A/3");
    expect(recommendedChords[1][1]).toEqual("C/4");
    expect(recommendedChords[1][2]).toEqual("E/4");
    // Adim
    expect(recommendedChords[2].length).toEqual(3);
    expect(recommendedChords[2][0]).toEqual("A/3");
    expect(recommendedChords[2][1]).toEqual("C/4");
    expect(recommendedChords[2][2]).toEqual("Eb/4");
    // Aaug
    expect(recommendedChords[3].length).toEqual(3);
    expect(recommendedChords[3][0]).toEqual("A/3");
    expect(recommendedChords[3][1]).toEqual("C#/4");
    expect(recommendedChords[3][2]).toEqual("F/4");
    // A(b5)
    expect(recommendedChords[4].length).toEqual(3);
    expect(recommendedChords[4][0]).toEqual("A/3");
    expect(recommendedChords[4][1]).toEqual("C#/4");
    expect(recommendedChords[4][2]).toEqual("Eb/4");
    // A5
    expect(recommendedChords[5].length).toEqual(2);
    expect(recommendedChords[5][0]).toEqual("A/3");
    expect(recommendedChords[5][1]).toEqual("E/4");
    // Asus
    expect(recommendedChords[6].length).toEqual(3);
    expect(recommendedChords[6][0]).toEqual("A/3");
    expect(recommendedChords[6][1]).toEqual("D/4");
    expect(recommendedChords[6][2]).toEqual("E/4");
    // Asus2
    expect(recommendedChords[7].length).toEqual(3);
    expect(recommendedChords[7][0]).toEqual("A/3");
    expect(recommendedChords[7][1]).toEqual("B/3");
    expect(recommendedChords[7][2]).toEqual("E/4");
    // A7
    expect(recommendedChords[8].length).toEqual(4);
    expect(recommendedChords[8][0]).toEqual("A/3");
    expect(recommendedChords[8][1]).toEqual("C#/4");
    expect(recommendedChords[8][2]).toEqual("E/4");
    expect(recommendedChords[8][3]).toEqual("G/4");
    // Am7
    expect(recommendedChords[9].length).toEqual(4);
    expect(recommendedChords[9][0]).toEqual("A/3");
    expect(recommendedChords[9][1]).toEqual("C/4");
    expect(recommendedChords[9][2]).toEqual("E/4");
    expect(recommendedChords[9][3]).toEqual("G/4");
    // A7sus
    expect(recommendedChords[10].length).toEqual(4);
    expect(recommendedChords[10][0]).toEqual("A/3");
    expect(recommendedChords[10][1]).toEqual("D/4");
    expect(recommendedChords[10][2]).toEqual("E/4");
    expect(recommendedChords[10][3]).toEqual("G/4");
    // A7(b5)
    expect(recommendedChords[11].length).toEqual(4);
    expect(recommendedChords[11][0]).toEqual("A/3");
    expect(recommendedChords[11][1]).toEqual("C#/4");
    expect(recommendedChords[11][2]).toEqual("Eb/4");
    expect(recommendedChords[11][3]).toEqual("G/4");
    // Am7(b5)
    expect(recommendedChords[12].length).toEqual(4);
    expect(recommendedChords[12][0]).toEqual("A/3");
    expect(recommendedChords[12][1]).toEqual("C/4");
    expect(recommendedChords[12][2]).toEqual("Eb/4");
    expect(recommendedChords[12][3]).toEqual("G/4");
    // Amaj7
    expect(recommendedChords[13].length).toEqual(4);
    expect(recommendedChords[13][0]).toEqual("A/3");
    expect(recommendedChords[13][1]).toEqual("C#/4");
    expect(recommendedChords[13][2]).toEqual("E/4");
    expect(recommendedChords[13][3]).toEqual("G#/4");
    // Am(maj7)
    expect(recommendedChords[14].length).toEqual(4);
    expect(recommendedChords[14][0]).toEqual("A/3");
    expect(recommendedChords[14][1]).toEqual("C/4");
    expect(recommendedChords[14][2]).toEqual("E/4");
    expect(recommendedChords[14][3]).toEqual("G#/4");
    // Amaj7(b5)
    expect(recommendedChords[15].length).toEqual(4);
    expect(recommendedChords[15][0]).toEqual("A/3");
    expect(recommendedChords[15][1]).toEqual("C#/4");
    expect(recommendedChords[15][2]).toEqual("Eb/4");
    expect(recommendedChords[15][3]).toEqual("G#/4");
    // A7+
    expect(recommendedChords[16].length).toEqual(4);
    expect(recommendedChords[16][0]).toEqual("A/3");
    expect(recommendedChords[16][1]).toEqual("C#/4");
    expect(recommendedChords[16][2]).toEqual("F/4");
    expect(recommendedChords[16][3]).toEqual("G/4");
    // A7+(b9)
    expect(recommendedChords[17].length).toEqual(5);
    expect(recommendedChords[17][0]).toEqual("A/3");
    expect(recommendedChords[17][1]).toEqual("C#/4");
    expect(recommendedChords[17][2]).toEqual("F/4");
    expect(recommendedChords[17][3]).toEqual("G/4");
    expect(recommendedChords[17][4]).toEqual("Bb/4");
    // A6
    expect(recommendedChords[18].length).toEqual(4);
    expect(recommendedChords[18][0]).toEqual("A/3");
    expect(recommendedChords[18][1]).toEqual("C#/4");
    expect(recommendedChords[18][2]).toEqual("E/4");
    expect(recommendedChords[18][3]).toEqual("F#/4");
    // A6/9
    expect(recommendedChords[19].length).toEqual(5);
    expect(recommendedChords[19][0]).toEqual("A/3");
    expect(recommendedChords[19][1]).toEqual("C#/4");
    expect(recommendedChords[19][2]).toEqual("E/4");
    expect(recommendedChords[19][3]).toEqual("F#/4");
    expect(recommendedChords[19][4]).toEqual("B/4");
    // Am6
    expect(recommendedChords[20].length).toEqual(4);
    expect(recommendedChords[20][0]).toEqual("A/3");
    expect(recommendedChords[20][1]).toEqual("C/4");
    expect(recommendedChords[20][2]).toEqual("E/4");
    expect(recommendedChords[20][3]).toEqual("F#/4");
    // Am6/9
    expect(recommendedChords[21].length).toEqual(5);
    expect(recommendedChords[21][0]).toEqual("A/3");
    expect(recommendedChords[21][1]).toEqual("C/4");
    expect(recommendedChords[21][2]).toEqual("E/4");
    expect(recommendedChords[21][3]).toEqual("F#/4");
    expect(recommendedChords[21][4]).toEqual("B/4");
    // A(add9)
    expect(recommendedChords[22].length).toEqual(4);
    expect(recommendedChords[22][0]).toEqual("A/3");
    expect(recommendedChords[22][1]).toEqual("C#/4");
    expect(recommendedChords[22][2]).toEqual("E/4");
    expect(recommendedChords[22][3]).toEqual("B/4");
    // A9
    expect(recommendedChords[23].length).toEqual(5);
    expect(recommendedChords[23][0]).toEqual("A/3");
    expect(recommendedChords[23][1]).toEqual("C#/4");
    expect(recommendedChords[23][2]).toEqual("E/4");
    expect(recommendedChords[23][3]).toEqual("G/4");
    expect(recommendedChords[23][4]).toEqual("B/4");
    // Amaj9
    expect(recommendedChords[24].length).toEqual(5);
    expect(recommendedChords[24][0]).toEqual("A/3");
    expect(recommendedChords[24][1]).toEqual("C#/4");
    expect(recommendedChords[24][2]).toEqual("E/4");
    expect(recommendedChords[24][3]).toEqual("G#/4");
    expect(recommendedChords[24][4]).toEqual("B/4");
    // Am9(maj7)
    expect(recommendedChords[25].length).toEqual(5);
    expect(recommendedChords[25][0]).toEqual("A/3");
    expect(recommendedChords[25][1]).toEqual("C/4");
    expect(recommendedChords[25][2]).toEqual("E/4");
    expect(recommendedChords[25][3]).toEqual("G#/4");
    expect(recommendedChords[25][4]).toEqual("B/4");
    // A7(b9)
    expect(recommendedChords[26].length).toEqual(5);
    expect(recommendedChords[26][0]).toEqual("A/3");
    expect(recommendedChords[26][1]).toEqual("C#/4");
    expect(recommendedChords[26][2]).toEqual("E/4");
    expect(recommendedChords[26][3]).toEqual("G/4");
    expect(recommendedChords[26][4]).toEqual("Bb/4");
    // A7(#9)
    expect(recommendedChords[27].length).toEqual(5);
    expect(recommendedChords[27][0]).toEqual("A/3");
    expect(recommendedChords[27][1]).toEqual("C#/4");
    expect(recommendedChords[27][2]).toEqual("E/4");
    expect(recommendedChords[27][3]).toEqual("G/4");
    expect(recommendedChords[27][4]).toEqual("C/5");
    // A9(#11)
    expect(recommendedChords[28].length).toEqual(6);
    expect(recommendedChords[28][0]).toEqual("A/3");
    expect(recommendedChords[28][1]).toEqual("C#/4");
    expect(recommendedChords[28][2]).toEqual("E/4");
    expect(recommendedChords[28][3]).toEqual("G/4");
    expect(recommendedChords[28][4]).toEqual("B/4");
    expect(recommendedChords[28][5]).toEqual("Eb/5");
    // A9+
    expect(recommendedChords[29].length).toEqual(5);
    expect(recommendedChords[29][0]).toEqual("A/3");
    expect(recommendedChords[29][1]).toEqual("C#/4");
    expect(recommendedChords[29][2]).toEqual("F/4");
    expect(recommendedChords[29][3]).toEqual("G/4");
    expect(recommendedChords[29][4]).toEqual("B/4");
    // A9(b5)
    expect(recommendedChords[30].length).toEqual(5);
    expect(recommendedChords[30][0]).toEqual("A/3");
    expect(recommendedChords[30][1]).toEqual("C#/4");
    expect(recommendedChords[30][2]).toEqual("Eb/4");
    expect(recommendedChords[30][3]).toEqual("G/4");
    expect(recommendedChords[30][4]).toEqual("B/4");
    // A11
    expect(recommendedChords[31].length).toEqual(6);
    expect(recommendedChords[31][0]).toEqual("A/3");
    expect(recommendedChords[31][1]).toEqual("C#/4");
    expect(recommendedChords[31][2]).toEqual("E/4");
    expect(recommendedChords[31][3]).toEqual("G/4");
    expect(recommendedChords[31][4]).toEqual("B/4");
    expect(recommendedChords[31][5]).toEqual("D/5");
    // Am11
    expect(recommendedChords[32].length).toEqual(6);
    expect(recommendedChords[32][0]).toEqual("A/3");
    expect(recommendedChords[32][1]).toEqual("C/4");
    expect(recommendedChords[32][2]).toEqual("E/4");
    expect(recommendedChords[32][3]).toEqual("G/4");
    expect(recommendedChords[32][4]).toEqual("B/4");
    expect(recommendedChords[32][5]).toEqual("D/5");
    // AM11
    expect(recommendedChords[33].length).toEqual(6);
    expect(recommendedChords[33][0]).toEqual("A/3");
    expect(recommendedChords[33][1]).toEqual("C#/4");
    expect(recommendedChords[33][2]).toEqual("E/4");
    expect(recommendedChords[33][3]).toEqual("G#/4");
    expect(recommendedChords[33][4]).toEqual("B/4");
    expect(recommendedChords[33][5]).toEqual("D/5");
    // A13
    expect(recommendedChords[34].length).toEqual(6);
    expect(recommendedChords[34][0]).toEqual("A/3");
    expect(recommendedChords[34][1]).toEqual("C#/4");
    expect(recommendedChords[34][2]).toEqual("E/4");
    expect(recommendedChords[34][3]).toEqual("G/4");
    expect(recommendedChords[34][4]).toEqual("B/4");
    expect(recommendedChords[34][5]).toEqual("F#/5");
    // A13(b9)
    expect(recommendedChords[35].length).toEqual(6);
    expect(recommendedChords[35][0]).toEqual("A/3");
    expect(recommendedChords[35][1]).toEqual("C#/4");
    expect(recommendedChords[35][2]).toEqual("E/4");
    expect(recommendedChords[35][3]).toEqual("G/4");
    expect(recommendedChords[35][4]).toEqual("Bb/4");
    expect(recommendedChords[35][5]).toEqual("F#/5");
    // A13(#9)
    expect(recommendedChords[36].length).toEqual(6);
    expect(recommendedChords[36][0]).toEqual("A/3");
    expect(recommendedChords[36][1]).toEqual("C#/4");
    expect(recommendedChords[36][2]).toEqual("E/4");
    expect(recommendedChords[36][3]).toEqual("G/4");
    expect(recommendedChords[36][4]).toEqual("C/5");
    expect(recommendedChords[36][5]).toEqual("F#/5");
    // A13(b9b5)
    expect(recommendedChords[37].length).toEqual(6);
    expect(recommendedChords[37][0]).toEqual("A/3");
    expect(recommendedChords[37][1]).toEqual("C#/4");
    expect(recommendedChords[37][2]).toEqual("Eb/4");
    expect(recommendedChords[37][3]).toEqual("G/4");
    expect(recommendedChords[37][4]).toEqual("Bb/4");
    expect(recommendedChords[37][5]).toEqual("F#/5");
})