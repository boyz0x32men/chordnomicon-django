import ProgressionBuilder from '../components/progressionbuilder/ProgressionBuilder'
import Progression from '../components/progressionbuilder/Progression'
import ProgressionTool from '../components/progressionbuilder/ProgressionTool'
import * as Utils from '../components/progressionbuilder/utils/progressionUtils'
import React from 'react'
import { mount } from 'enzyme'
import Vex from 'vexflow'
import ProgressionSubTool from '../components/progressionbuilder/ProgressionSubTool';

const VF = Vex.Flow

describe('Progression Builder Test Lifecycle Methods', () => {
    let wrapper
    beforeEach(() => {
        const { progressionBuilderEnzymeWrapper } = mountSetup()
        wrapper = progressionBuilderEnzymeWrapper
    })

    it('should render div', () => {
        wrapper.find('#progtools')
        wrapper.find('#progshow')
    })

    it('should change state on resize event', () => {
        expect(wrapper.instance().state.width).toEqual(924)
        global.innerWidth = 1920
        global.dispatchEvent(new Event('resize'))
        expect(wrapper.instance().state.width).toEqual(1820)
    })

    it('should create a reactref', () => {
        expect(wrapper.instance().tool.current).toBeInstanceOf(ProgressionTool)
    })

    
})

describe('Progression Test passing props', () => {
    let wrapper

    beforeEach(() => {
        const { progressionEnzymeWrapper } = mountSetup()
        wrapper = progressionEnzymeWrapper
    })

    it('Does Mount', () => {
        // Renders Div for showing the Progression Built
        expect(wrapper.find('div'))
    })

    it('Pass Props', () => {
        expect(wrapper.instance().props.width).toEqual(1024)
    }) 
    
    it('Draws a stave', () => {
        expect(wrapper.find('svg'))
    })
})

describe('Test Progression Initialization Methods for VexFlow', () => {
    let wrapper 

    beforeEach(() => {
        const { progressionEnzymeWrapper } = mountSetup()
        wrapper = progressionEnzymeWrapper
    })

    it('should initialize Vex Renderer ', () => {
        expect(wrapper.instance().renderer).toBeInstanceOf(VF.Renderer)
    })

    it('should get SVGContext from renderer', () => {
        // when checking with toBeInstanceOf of returns "undefined", but console logging says otherwise
        expect(wrapper.instance().context).toBeDefined(VF.SVGContext)
    })

    it('should find the vexflow svg', () => {
        expect(wrapper.find('svg'))
    })

    it('should initialize a voice', () => {
        console.log = jest.fn()
        wrapper.instance().blankVoice()
        expect(wrapper.instance().voice.time.num_beats).toEqual(0)
    })

    it('should increment num_beats by 1', () => {
        wrapper.instance().blankVoice()
        wrapper.instance().addChord("C/4")
        expect(wrapper.instance().voice.time.num_beats).toEqual(1)
        wrapper.instance().addChord("C/4")
        expect(wrapper.instance().voice.time.num_beats).toEqual(2)
    })

    it('should save/reload properly Progression', () => {
        var expected = JSON.stringify([["c/4"], ["d/5"]])
        wrapper.instance().notes = [["c/4"], ["d/5"]]
        wrapper.instance().saveProgressionToJSON()
        expect(wrapper.instance().progressionJSON).toEqual(expected)
        wrapper.instance().reloadProgressionFromJSON()
        expect(wrapper.instance().notes).toEqual([["c/4"], ["d/5"]])
    })

    it('should remove last', () => {
        wrapper.instance().drawChords = jest.fn()
        wrapper.instance().notes = [["c/4"], ["d/5"]]
        wrapper.instance().removeLast()
        expect(wrapper.instance().notes.length).toEqual(1)
    })

    it('should swap chords at index', () => {
        wrapper.instance().drawChords = jest.fn()
        wrapper.instance().notes = [["c/4"], ["d/5"]]
        wrapper.instance().swapChords(1,2)
        expect(wrapper.instance().notes[0]).toEqual(["d/5"]);
    })

    it('should perform operations on vexflow Voice', () => {
        wrapper.instance().blankVoice()
        expect(wrapper.instance().voice.time.num_beats).toEqual(0)
        wrapper.instance().incrementVoice()
        expect(wrapper.instance().voice.time.num_beats).toEqual(1)
        wrapper.instance().specifiedVoice(69)
        expect(wrapper.instance().voice.time.num_beats).toEqual(69)
        wrapper.instance().decrementVoice()
        expect(wrapper.instance().voice.time.num_beats).toEqual(68)
    })
})

describe('Progression Tool Test Lifecycle Methods of ', () => {
    let wrapper, refWrapper
    beforeAll(() => {
        const { toolEnzymeWrapper, ref } = mountSetup()
        wrapper = toolEnzymeWrapper
        refWrapper = ref
    })

    it('should have created many refs', () => {
        expect(wrapper.instance().keyOrModeTool).toBeTruthy()
        expect(wrapper.instance().addChordTool).toBeTruthy()
        expect(wrapper.instance().modifyChordTool).toBeTruthy()
        expect(wrapper.instance().changeInstrumentTool).toBeTruthy()
        expect(wrapper.instance().clearProgressionTool).toBeTruthy()
    })
})

describe('Progression Sub Tool Test List populate functions', () => {
    let wrapper, keyInput, modeInput
    beforeAll(() => {
        const { subToolEnzymeWrapper } = mountSetup(1)
        wrapper = subToolEnzymeWrapper
        keyInput = wrapper.find('#keyInput')
    })

    it('should return an options list', () => {
        expect(wrapper.instance().createSelectItems(null)).toBe(undefined)
        expect(wrapper.instance().createSelectItems(Utils.chords).length).toEqual(2)
    })

})

describe('Progression Utils Testing Functions', () => {
    let inputKey = "C/4", invalidInputKey = "c4"

    it('Should return inputKey back since of valid form', () => {
        window.alert = jest.fn()
        expect(Utils.checkValidVexForm(inputKey)).toBeTruthy
        // Test Alert user to invalid form
        expect(Utils.checkValidVexForm(invalidInputKey)).toBeFalsy
    })

    it('should return the appropriate integer for duration letter', () => {
       var foo = Utils.letterDurationToInt('w')
       var bar = Utils.letterDurationToInt('q')
       var flubar = Utils.letterDurationToInt('h')
       var hiDrBeaty = Utils.letterDurationToInt('z')
       expect(foo).toEqual(1)
       expect(bar).toEqual(4)
       expect(flubar).toEqual(2)
       expect(hiDrBeaty).toEqual(null)
    })
})

function mountSetup(tab) {
    const props = {
        width: 1024,
        ops: tab
    }
    const ref = React.createRef()
    const progressionBuilderEnzymeWrapper = mount(<ProgressionBuilder />)
    const progressionEnzymeWrapper = mount(<Progression {... props} />)
    const toolEnzymeWrapper = mount(<ProgressionTool />)
    const subToolEnzymeWrapper = mount(<ProgressionSubTool {... props} />)
    return {
        progressionBuilderEnzymeWrapper, progressionEnzymeWrapper, toolEnzymeWrapper, ref, subToolEnzymeWrapper
    }
}