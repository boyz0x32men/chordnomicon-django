import * as actions from "../store/actions/auth";
import * as types from "../store/actions/types";

describe('actions', () => {
  it('should create AUTH_START', () => {
    const expectedAction = {
        type: types.AUTH_START
    }
    expect(actions.authStart()).toEqual(expectedAction);
  });
  it('should create AUTH_SUCCESS', () => {
    const token = 'd6beed2a7ed037521a4ebfaf41cab627a7588a87';
    const expectedAction = {
        type: types.AUTH_SUCCESS,
        token: token
    }
    expect(actions.authSuccess(token)).toEqual(expectedAction);
  });
  it('should create AUTH_FAIL', () => {
    const error = 'ERROR';
    const expectedAction = {
        type: types.AUTH_FAIL,
        error: error
    }
    expect(actions.authFail(error)).toEqual(expectedAction);
  });
  it('should create AUTH_LOGOUT', () => {
    const expectedAction = {
        type: types.AUTH_LOGOUT
    }
    expect(actions.logout()).toEqual(expectedAction);
  });
});
