import React from 'react';
import ReactDOM from 'react-dom';
import ChordBuilder from '../components/chordbuilder/ChordBuilder';

it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<ChordBuilder />, div);
    ReactDOM.unmountComponentAtNode(div);
});