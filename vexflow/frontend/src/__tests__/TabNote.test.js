import React from 'react'
import ReactDOM from 'react-dom'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'
import {Component} from 'react';
import TabNote from '../components/scorebuilder/TabNote';

var testNote = new TabNote({note: {content:[]}, score: null, clef: null})

it('should gen the correct tab type', () => {
    var type = testNote.getType("whole")
    expect(type).toBe("w")
    type = testNote.getType("half")
    expect(type).toBe("h")
    type = testNote.getType("quarter")
    expect(type).toBe("q")
    type = testNote.getType("eighth")
    expect(type).toBe("8")
    type = testNote.getType("16th")
    expect(type).toBe("16")
    type = testNote.getType("32nd")
    expect(type).toBe("32")
    type = testNote.getType("64th")
    expect(type).toBe("64")
    type = testNote.getType("default")
    expect(type).toBe(null)
});
