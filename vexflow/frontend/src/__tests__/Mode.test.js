import Mode from '../components/Mode';
import ModeFactory from '../components/ModeFactory';
import IntervalFactory from '../components/IntervalFactory';

let mode = ModeFactory.getModeByName("Major");

it('test getName', () => {
    expect(mode.getName()).toEqual("Major");
})

it('test setName', () => {
    let newMode = new Mode();
    newMode.setName("New Mode");
    expect(newMode.getName()).toEqual("New Mode");
}) 

it('test addInterval', () => {
    let newMode = new Mode()
    newMode.addInterval(IntervalFactory.getIntervalByName("R"));
    expect(newMode.getSize()).toEqual(1);
})

it('test getSize', () => {
    expect(mode.getSize()).toEqual(7);
})

it('test getIntervals', () => {
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, M6, M7");
})

it('test getIntervalValue', () => {
    expect(mode.getIntervalValue(0)).toEqual(0);
    expect(mode.getIntervalValue(1)).toEqual(2);
    expect(mode.getIntervalValue(2)).toEqual(4);
    expect(mode.getIntervalValue(3)).toEqual(5);
    expect(mode.getIntervalValue(4)).toEqual(7);
    expect(mode.getIntervalValue(5)).toEqual(9);
    expect(mode.getIntervalValue(6)).toEqual(11);
})

it('test getIntervalName', () => {
    expect(mode.getIntervalName(0)).toEqual("R");
    expect(mode.getIntervalName(1)).toEqual("M2");
    expect(mode.getIntervalName(2)).toEqual("M3");
    expect(mode.getIntervalName(3)).toEqual("P4");
    expect(mode.getIntervalName(4)).toEqual("P5");
    expect(mode.getIntervalName(5)).toEqual("M6");
    expect(mode.getIntervalName(6)).toEqual("M7");
})

it('test containsIntervalName', () => {
    expect(mode.containsIntervalName("R")).toEqual(true);
    expect(mode.containsIntervalName("M2")).toEqual(true);
    expect(mode.containsIntervalName("M3")).toEqual(true);
    expect(mode.containsIntervalName("P4")).toEqual(true);
    expect(mode.containsIntervalName("P5")).toEqual(true);
    expect(mode.containsIntervalName("M6")).toEqual(true);
    expect(mode.containsIntervalName("M7")).toEqual(true);
})

it('test containsIntervalValue', () => {
    expect(mode.containsIntervalValue(0)).toEqual(true);
    expect(mode.containsIntervalValue(2)).toEqual(true);
    expect(mode.containsIntervalValue(4)).toEqual(true);
    expect(mode.containsIntervalValue(5)).toEqual(true);
    expect(mode.containsIntervalValue(7)).toEqual(true);
    expect(mode.containsIntervalValue(9)).toEqual(true);
    expect(mode.containsIntervalValue(11)).toEqual(true);
})

/*
it('test containsNote', () => {
    expect(mode.containsNote(NoteFactory.getNoteByName("D", "C"))).toEqual(true);
})
*/

it('test containsNoteName', () => {
    expect(mode.containsNoteName("C", "C")).toEqual(true);
    expect(mode.containsNoteName("D", "C")).toEqual(true);
    expect(mode.containsNoteName("E", "C")).toEqual(true);
    expect(mode.containsNoteName("F", "C")).toEqual(true);
    expect(mode.containsNoteName("G", "C")).toEqual(true);
    expect(mode.containsNoteName("A", "C")).toEqual(true);
    expect(mode.containsNoteName("B", "C")).toEqual(true);
})

it('test getNote', () => {
    expect(mode.getNote(0, "C")).toEqual("C");
    expect(mode.getNote(1, "C")).toEqual("D");
    expect(mode.getNote(2, "C")).toEqual("E");
    expect(mode.getNote(3, "C")).toEqual("F");
    expect(mode.getNote(4, "C")).toEqual("G");
    expect(mode.getNote(5, "C")).toEqual("A");
    expect(mode.getNote(6, "C")).toEqual("B");
})