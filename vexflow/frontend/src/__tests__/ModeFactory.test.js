import ModeFactory from '../components/ModeFactory';

// Basic tests
it('test Major mode construction', () => {
    let mode = ModeFactory.getModeByName("Giberish");
    expect(mode).toEqual(null);
})
it('test Major mode construction', () => {
    let mode = ModeFactory.getModeByName("Major");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, M6, M7");
    expect(mode.getNote(0, "C")).toEqual("C");
    expect(mode.getNote(1, "C")).toEqual("D");
    expect(mode.getNote(2, "C")).toEqual("E");
    expect(mode.getNote(3, "C")).toEqual("F");
    expect(mode.getNote(4, "C")).toEqual("G");
    expect(mode.getNote(5, "C")).toEqual("A");
    expect(mode.getNote(6, "C")).toEqual("B");
})

// Heptatonic modes
it('test Lydian mode construction', () => {
    let mode = ModeFactory.getModeByName("Lydian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, P5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Ionian mode construction', () => {
    let mode = ModeFactory.getModeByName("Ionian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Mixolydian mode construction', () => {
    let mode = ModeFactory.getModeByName("Mixolydian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Dorian mode construction', () => {
    let mode = ModeFactory.getModeByName("Dorian");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, P4, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Aeolian mode construction', () => {
    let mode = ModeFactory.getModeByName("Aeolian");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, P4, P5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Phrygian mode construction', () => {
    let mode = ModeFactory.getModeByName("Phrygian");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, P5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Locrian mode construction', () => {
    let mode = ModeFactory.getModeByName("Locrian");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Lydian Augmented mode construction', () => {
    let mode = ModeFactory.getModeByName("Lydian Augmented");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, aug5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    //expect(mode.getNote(4, "A")).toEqual("E#"); Need to change note naming convention to include aug5
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Acoustic mode construction', () => {
    let mode = ModeFactory.getModeByName("Acoustic");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Half Diminished mode construction', () => {
    let mode = ModeFactory.getModeByName("Half Diminished");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, P4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Harmonic Mixolydian mode construction', () => {
    let mode = ModeFactory.getModeByName("Harmonic Mixolydian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Altered mode construction', () => {
    let mode = ModeFactory.getModeByName("Altered");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, dim4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("Db"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Jazz Melodic Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Jazz Melodic Minor");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, P4, P5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Harmonic Phrygian mode construction', () => {
    let mode = ModeFactory.getModeByName("Harmonic Phrygian");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Super Augmented mode construction', () => {
    let mode = ModeFactory.getModeByName("Super Augmented");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, aug5, aug6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    //expect(mode.getNote(4, "A")).toEqual("E#"); Need to change note naming convention to include aug5
    //expect(mode.getNote(5, "A")).toEqual("F##"); Need to change note naming convention to include aug6
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Mixolydian Augmented mode construction', () => {
    let mode = ModeFactory.getModeByName("Mixolydian Augmented");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, aug5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    //expect(mode.getNote(4, "A")).toEqual("E#"); Need to change note naming convention to include aug5
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Minor Lydian mode construction', () => {
    let mode = ModeFactory.getModeByName("Minor Lydian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, aug4, P5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Major Locrian mode construction', () => {
    let mode = ModeFactory.getModeByName("Major Locrian");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Aeolian Diminished mode construction', () => {
    let mode = ModeFactory.getModeByName("Aeolian Diminished");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, dim4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("Db"); Need to change note naming convention to include dim4
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Super Diminished mode construction', () => {
    let mode = ModeFactory.getModeByName("Super Diminished");
    expect(mode.getIntervalNames()).toEqual("R, m2, dim3, dim4, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    //expect(mode.getNote(2, "A")).toEqual("Cb"); Need to change note naming convention to include dim3
    //expect(mode.getNote(3, "A")).toEqual("Db"); Need to change note naming convention to include dim4
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Neapolitan mode construction', () => {
    let mode = ModeFactory.getModeByName("Neapolitan");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, P5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Hungarian Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Hungarian Minor");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, aug4, P5, m6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Double Harmonic mode construction', () => {
    let mode = ModeFactory.getModeByName("Double Harmonic");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, P4, P5, m6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Harmonic Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Harmonic Minor");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, P5, m6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Ukrainian Dorian mode construction', () => {
    let mode = ModeFactory.getModeByName("Ukrainian Dorian");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, aug4, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Phrygian Dominant mode construction', () => {
    let mode = ModeFactory.getModeByName("Phrygian Dominant");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, P4, P5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Harmonic Major mode construction', () => {
    let mode = ModeFactory.getModeByName("Harmonic Major");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, aug6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    //expect(mode.getNote(5, "A")).toEqual("G"); Change note naming conventions to include aug6
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Pfluke mode construction', () => {
    let mode = ModeFactory.getModeByName("Pfluke");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, aug4, P5, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Neapolitan Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Neapolitan Minor");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, P4, P5, m6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Enigmatic mode construction', () => {
    let mode = ModeFactory.getModeByName("Enigmatic");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, aug4, aug5, aug6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    //expect(mode.getNote(3, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    //expect(mode.getNote(4, "A")).toEqual("E#"); Need to change note naming convention to include aug5
    //expect(mode.getNote(5, "A")).toEqual("F##"); Need to change note naming convention to include aug6
    expect(mode.getNote(6, "A")).toEqual("G#");
})

it('test Persian mode construction', () => {
    let mode = ModeFactory.getModeByName("Persian");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, P4, dim5, dim6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("E");
    expect(mode.getNote(6, "A")).toEqual("G");
})

it('test Blues with Flat-Four mode construction', () => {
    let mode = ModeFactory.getModeByName("Blues with Flat-Four");
    expect(mode.getIntervalNames()).toEqual("R, m3, dim4, P4, dim5, P5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("C");
    //expect(mode.getNote(2, "A")).toEqual("Db"); Need to change note naming convention to include dim4
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("E");
    expect(mode.getNote(6, "A")).toEqual("G");
})

// Pentatonic modes
it('test Pentatonic Major mode construction', () => {
    let mode = ModeFactory.getModeByName("Pentatonic Major");
    expect(mode.getIntervalNames()).toEqual("R, M2, P4, P5, M6");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("F#");
})

it('test Pentatonic Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Pentatonic Minor");
    expect(mode.getIntervalNames()).toEqual("R, m3, P4, P5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("C");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("G");
})

it('test Suspended mode construction', () => {
    let mode = ModeFactory.getModeByName("Suspended");
    expect(mode.getIntervalNames()).toEqual("R, M2, P4, P5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("G");
})

it('test Blues Major mode construction', () => {
    let mode = ModeFactory.getModeByName("Blues Major");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P5, M6");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("F#");
})

it('test Blues Minor mode construction', () => {
    let mode = ModeFactory.getModeByName("Blues Minor");
    expect(mode.getIntervalNames()).toEqual("R, m3, P4, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("C");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("F");
    expect(mode.getNote(4, "A")).toEqual("G");
})

it('test Hirajoshi mode construction', () => {
    let mode = ModeFactory.getModeByName("Hirajoshi");
    expect(mode.getIntervalNames()).toEqual("R, M3, aug4, P5, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("C#");
    //expect(mode.getNote(2, "A")).toEqual("D#"); Need to change note naming convention to include aug4
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("G#");
})

it('test In mode construction', () => {
    let mode = ModeFactory.getModeByName("In");
    expect(mode.getIntervalNames()).toEqual("R, m2, P4, P5, m6");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("F");
})

it('test Iwato mode construction', () => {
    let mode = ModeFactory.getModeByName("Iwato");
    expect(mode.getIntervalNames()).toEqual("R, m2, P4, dim5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("Eb");
    expect(mode.getNote(4, "A")).toEqual("G");
})

it('test Insen mode construction', () => {
    let mode = ModeFactory.getModeByName("Insen");
    expect(mode.getIntervalNames()).toEqual("R, m2, P4, P5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("G");
})

//Hexatonic Modes
it('test Blues mode construction', () => {
    let mode = ModeFactory.getModeByName("Blues");
    expect(mode.getIntervalNames()).toEqual("R, m3, P4, dim5, P5, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("C");
    expect(mode.getNote(2, "A")).toEqual("D");
    expect(mode.getNote(3, "A")).toEqual("Eb");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("G");
})

it('test Whole Step mode construction', () => {
    let mode = ModeFactory.getModeByName("Whole Step");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, dim5, m6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("Eb");
    expect(mode.getNote(4, "A")).toEqual("F");
    expect(mode.getNote(5, "A")).toEqual("G");
})

it('test Augmented mode construction', () => {
    let mode = ModeFactory.getModeByName("Augmented");
    expect(mode.getIntervalNames()).toEqual("R, aug2, M3, P5, m6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    //expect(mode.getNote(1, "A")).toEqual("B#"); Need to change note naming convention to include aug2
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("E");
    expect(mode.getNote(4, "A")).toEqual("F");
    expect(mode.getNote(5, "A")).toEqual("G#");
})

it('test Hexatonic Diminished mode construction', () => {
    let mode = ModeFactory.getModeByName("Hexatonic Diminished");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, P4, m6, dim7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("F");
    //expect(mode.getNote(5, "A")).toEqual("Gb"); Need to change note naming convention to include dim7
})

it('test Tritone mode construction', () => {
    let mode = ModeFactory.getModeByName("Tritone");
    expect(mode.getIntervalNames()).toEqual("R, m2, M3, dim5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("Eb");
    expect(mode.getNote(4, "A")).toEqual("F#");
    expect(mode.getNote(5, "A")).toEqual("G");
})

it('test Prometheus mode construction', () => {
    let mode = ModeFactory.getModeByName("Prometheus");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, dim5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("Eb");
    expect(mode.getNote(4, "A")).toEqual("F#");
    expect(mode.getNote(5, "A")).toEqual("G");
})

it('test Istran mode construction', () => {
    let mode = ModeFactory.getModeByName("Istran");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, dim4, dim5, dim6");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("Db"); Need to change note naming convention to include dim4
    expect(mode.getNote(4, "A")).toEqual("Eb");
    //expect(mode.getNote(5, "A")).toEqual("Fb"); Need to change note naming convention to include dim6
})

//Octatonic Modes
it('test Diminished Whole-Half mode construction', () => {
    let mode = ModeFactory.getModeByName("Diminished Whole-Half");
    expect(mode.getIntervalNames()).toEqual("R, M2, m3, P4, dim5, m6, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("F#");
    expect(mode.getNote(7, "A")).toEqual("G#");
})

it('test Diminished Half-Whole mode construction', () => {
    let mode = ModeFactory.getModeByName("Diminished Half-Whole");
    expect(mode.getIntervalNames()).toEqual("R, m2, m3, dim4, dim5, P5, M6, m7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("C");
    //expect(mode.getNote(3, "A")).toEqual("Db"); Need to change note naming convention to include dim4
    expect(mode.getNote(4, "A")).toEqual("Eb");
    expect(mode.getNote(5, "A")).toEqual("E");
    expect(mode.getNote(6, "A")).toEqual("F#");
    expect(mode.getNote(7, "A")).toEqual("G");
})

it('test Major Bebop mode construction', () => {
    let mode = ModeFactory.getModeByName("Major Bebop");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, m6, M6, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F");
    expect(mode.getNote(6, "A")).toEqual("F#");
    expect(mode.getNote(7, "A")).toEqual("G#");
})

it('test Bebop Dominant mode construction', () => {
    let mode = ModeFactory.getModeByName("Bebop Dominant");
    expect(mode.getIntervalNames()).toEqual("R, M2, M3, P4, P5, M6, m7, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("B");
    expect(mode.getNote(2, "A")).toEqual("C#");
    expect(mode.getNote(3, "A")).toEqual("D");
    expect(mode.getNote(4, "A")).toEqual("E");
    expect(mode.getNote(5, "A")).toEqual("F#");
    expect(mode.getNote(6, "A")).toEqual("G");
    expect(mode.getNote(7, "A")).toEqual("G#");
})

it('test Chromatic mode construction', () => {
    let mode = ModeFactory.getModeByName("Chromatic");
    expect(mode.getIntervalNames()).toEqual("R, m2, M2, m3, M3, P4, dim5, P5, m6, M6, m7, M7");
    expect(mode.getNote(0, "A")).toEqual("A");
    expect(mode.getNote(1, "A")).toEqual("Bb");
    expect(mode.getNote(2, "A")).toEqual("B");
    expect(mode.getNote(3, "A")).toEqual("C");
    expect(mode.getNote(4, "A")).toEqual("C#");
    expect(mode.getNote(5, "A")).toEqual("D");
    expect(mode.getNote(6, "A")).toEqual("Eb");
    expect(mode.getNote(7, "A")).toEqual("E");
    expect(mode.getNote(8, "A")).toEqual("F");
    expect(mode.getNote(9, "A")).toEqual("F#");
    expect(mode.getNote(10, "A")).toEqual("G");
    expect(mode.getNote(11, "A")).toEqual("G#");
})