import React from 'react'
import ReactDOM from 'react-dom'
import Score from '../components/scorebuilder/Score'
import testObject from '../components/scorebuilder/musicjson/testObject'
import helloWorld from '../components/scorebuilder/musicjson/helloWorld'
import { shallow, mount } from 'enzyme'
import sinon from 'sinon'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Score score={testObject}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});

describe('Score Method Spies', () => {

	it('call to componentDidMount', () => {
		// spy replaces method in any instace of Score being mounted
		sinon.spy(Score.prototype, 'componentDidMount');
		// mount instance of Score componenet
		const wrapper = mount( <Score score={testObject}/> );
		// spy replaces the method with
		expect(Score.prototype.componentDidMount).toHaveProperty('callCount', 1);
		// unmount because it told me so
		wrapper.unmount();
	});

	it('call to createScore() ', () => {
		sinon.spy(Score.prototype, 'parseParts')
		const wrapper = mount( <Score score={helloWorld}/> );
		expect(Score.prototype.parseParts).toHaveProperty('callCount', 1);
		wrapper.unmount();
	});

	it('call to clear() ', () => {
		sinon.spy(Score.prototype, 'clear')
		const wrapper = mount( <Score score={helloWorld}/> );
		expect(Score.prototype.clear).toHaveProperty('callCount', 1);
		wrapper.unmount();
	});


});
