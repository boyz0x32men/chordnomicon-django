export default class Interval
    {
        constructor(_intervalValue, _name)
        {
            this.intervalValue = _intervalValue;
            this.name = _name;
        }
        getIntervalValue() { return this.intervalValue; }
        getName() { return this.name; }
    }