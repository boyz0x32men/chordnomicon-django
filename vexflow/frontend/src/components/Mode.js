import Vex from 'vexflow';
import {Component} from 'react';
import NoteFactory from './NoteFactory';

export default class Mode extends Component {
	constructor(props){
        super(props);
        this.name = null;
        this.intervals = [];
    }

    setName (name) { this.name = name; }
    getName () { return this.name; }
    addInterval(interval) { this.intervals[this.intervals.length] = interval; }
    getSize() { return this.intervals.length; }
    getIntervalNames()
    {
        let names = "";
        for (let i = 0; i < this.intervals.length; i++)
        {
            names = names + this.intervals[i].name;
            if (i < this.intervals.length - 1) { names = names + ", "; }
        }
        return names;
    }
    getIntervalValue(position)
    {
        return this.intervals[position].getIntervalValue();
    }
    getIntervalName(position)
    {
        return this.intervals[position].getName();
    }
    containsIntervalName(intervalName)
    {
        for (let i = 0; i < 7; i++)
        {
            if(this.intervals[i].getName() === intervalName) { return true; }
        }
        return false;
    }
    containsIntervalValue(intervalValue)
    {
        while(intervalValue >= 12) {intervalValue = intervalValue - 12;}
        while(intervalValue < 0) {intervalValue = intervalValue + 12;}
        for (let i = 0; i < this.getSize(); i++)
        {
            if (this.intervals[i].getIntervalValue() === intervalValue) { return true; }
        }
        return false;
    }
    /*
    containsNote(note, key)
    {
        let distance = note.getValue() - key.getValue();
        if (distance < 0) { distance = distance + 12; }
        for (let i = 0; i < this.getSize(); i++)
        {
            if (NoteFactory.getIntervalValue(this.intervals[i]) === distance) { return true; }
        }
        return false;
    }
    */
    containsNoteName(note, key)
    {
        for (let i = 0; i < this.getSize(); i++)
        {
            if (this.getNote(i, key) === note) { return true; }
        }
        return false;
    }
    getNote(index, key)
    {
        return NoteFactory.getNoteByValue(this.intervals[index].getIntervalValue(), key);
    }
}