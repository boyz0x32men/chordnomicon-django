import React, { Component } from "react"
import ProgressionTool from './ProgressionTool'
import Progression from "./Progression";

export default class ProgressionBuilder extends Component {
    constructor(props) {
        super(props)
        this.tool = React.createRef()
        this.progression = React.createRef()
        this.state = {
            width: window.innerWidth - 100
        }
    }

    componentDidMount() {
        window.addEventListener('resize', this.windowResized)
    }

    windowResized = () => {
        this.setState({ width: (window.innerWidth - 100) });
    }

    render() {
        return (
            <div className="Progression-buider">
                <div id="progtools" className="progression-toolbar">
                    <ProgressionTool ref={this.tool} progression={this.progression} />
                </div>
                <div id="progshow" className="progression-display">
                    <Progression ref = {this.progression} width={this.state.width} tool={this.tool}/>
                </div>            
            </div>);
    }
}