import React, { Component } from "react";
import Vex from 'vexflow'
import ChordFactory from "../ChordFactory";

const { Formatter, Stave, Renderer, Voice, StaveNote } = Vex.Flow

export default class Progression extends Component {
    constructor(props) {
        super(props)
        this.notes = []
        this.vexNotes = []
        this.stave = null
        this.renderer = null
        this.context = null
        this.voice = null
        this.beatDuration = 4
        this.width = this.props.width
        this.tool = this.props.tool
        this.progressionJSON = null;
    }

    componentDidMount() {
        this.createProgressionContainer()
    }

    createProgressionContainer() {
        this.initializeVex()
        this.blankVoice()
        this.drawProgressionStave()
    }
    
    initializeVex() {
        this.renderer = new Renderer(this.refs.outer, Renderer.Backends.SVG);
        this.context = this.renderer.getContext();
    }

    drawProgressionStave = () => {
        this.stave = new Stave(100, 0 , this.width-100)
        this.stave.setContext(this.context).draw()
        this.renderer.resize( this.width, 200)
        
    }

    blankVoice = () => {
        this.voice = new Voice({num_beats: 0, beat_value: this.beatDuration})
    }

    incrementVoice = () => {
        var newNumBeats = this.voice.time.num_beats + 1
        this.voice = new Voice({num_beats: newNumBeats, beat_value: this.beatDuration})
    }

    decrementVoice = () => {
        var newNumBeats = this.voice.time.num_beats - 1
        this.voice = new Voice({num_beats: newNumBeats, beat_value: this.beatDuration})
    }

    specifiedVoice = (size) => {
        this.voice = new Voice({num_beats: size, beat_value: this.beatDuration})
    }

    refreshVoice = () => {
        //reinitialize voice when making inplace edits of chords
        this.voice = new Voice({num_beats: this.voice.time.num_beats, beat_value: this.beatDuration})
    }

    addChord = (chordName) => {
        this.clearStave()
        //numbeats are the amount of beats(notes/chords/rests) in the voice
        //the beat value signifies the duration of the notes
        this.incrementVoice()
        var input = chordName.split("/")
        var chord = ChordFactory.getChordByName(input[0], input[1])
        // console.log(chord)
        this.notes.push(chord)
        this.createStaveNotes()
        this.drawChords()
    }

    swapChords = (a, b) => {
        if(this.notes.length >= 2){
            var temp = this.notes[a-1]
            this.notes[a-1] = this.notes[b-1]
            this.notes[b-1] = temp
            this.clearStave()
            this.createStaveNotes()
            this.refreshVoice()
            console.log(this.vexNotes)
            this.drawChords()
        } 
    }


    drawChords = () => {
        this.voice.addTickables(this.vexNotes)
        var formatter = new Formatter().joinVoices([this.voice]).format([this.voice], this.width-150)
        this.voice.draw(this.context, this.stave) 
    }

    createStaveNotes = () => {
        for( let note of this.notes) {
            var push = new StaveNote({clef:'treble', duration:"q", keys:note })
            this.vexNotes.push(push)
        }
    }

    clearNotes = () => {
        this.notes = []
    }

    clearStave = () => {
        for( let note of this.vexNotes){
            note.getAttribute('el').remove()
        }
        this.vexNotes = []
    }

    clearEverything = () => {
        this.clearNotes()
        this.clearStave()
        this.blankVoice()
    }

    removeLast = () => {
        if(this.notes.length >= 1){
            this.notes.pop()
            this.decrementVoice()
            this.clearStave()
            this.createStaveNotes()
            this.drawChords()
        }
    }

    reloadProgressionFromJSON = () => {
        this.notes = JSON.parse(this.progressionJSON)
        this.specifiedVoice(this.notes.length)
        this.createStaveNotes()
        this.drawChords()
        this.progressionJSON = null;
    }

    saveProgressionToJSON = () => {
        this.progressionJSON = JSON.stringify(this.notes)
        this.clearEverything()
        console.log(this.progressionJSON)
    }

    render() {
        return <div className='progression' ref='outer' ></div>
    }
}