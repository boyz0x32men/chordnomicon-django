export const keys = ['Ab','A','A#','Bb','B','B#','C','C#','Db','D','D#','Eb','E','F','F#','Gb','G','G#']

export const mode_types = {
    "Pentatonic": ["Blues Major", "Pentatonic Major", "Suspended", "Pentatonic Minor", "Blues Minor", "Hirajoshi", "In", "Iwato", "Insen"], 
    "Hexatonic": ["Blues", "Whole Tone","Augmented","Hexatonic Diminished","Tritone","Prometheus","Istran"], 
    "Heptatonic": ["Lydian", "Ionian (Major)","Mixolydian","Dorian","Aeolian (Minor)","Phrygian","Locrian","Lydian Augmented","Lydian Flat-Seven","HarmoniMixolydian","Half Diminished","Altered","Melodic Minor","Harmonic Phrygian",], 
    "Heptatonic Alternative": ["Neapolitan Major","Super Augmented","Mixolydia Augmented", "Minor Lydian", "Major Locrian","Aeolian Diminished","SupeDiminished","Hungarian Minor","Double Harmonic","Harmonic Minor","Pfluke", "Neapolitan Minor","Enigmatic","Persian","Blues with Flat-Four"],
    "Octonic": ["Diminished Whole-Half",
    "Diminished Half-Whole",
    "Major Bebop",
    "Bebop Dominant"]
}

export const chords = [["a/4", "d/5", "e/5"], ["b/4", "d/5", "g/5"]]

export function checkValidVexForm(key){
    let split = key.split("/")
    if(split.length > 1){
        return true
    }
    else {
        return false
    }
}

export function letterDurationToInt(duration){
    switch (duration) {
        case 'w':
            return 1
        case 'q':
            return 4
        case 'h':
            return 2
        default:
            return null
    }
}


