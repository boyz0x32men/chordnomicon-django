import React, { Component } from 'react'
import { Tabs, Tab } from 'react-bootstrap';
import ProgressionSubTool from './ProgressionSubTool';
export default class ProgressionTool extends Component {
    constructor(props){
        super(props)
        this.currentTool = "1"
        this.progression = this.props.progression
        this.keyOrModeTool = React.createRef()
        this.addChordTool = React.createRef()
        this.modifyChordTool = React.createRef()
        this.changeInstrumentTool = React.createRef()
        this.clearProgressionTool = React.createRef()
        this.toolMap = {
            "1": this.keyOrModeTool,
            "2": this.addChordTool,
            "3": this.modifyChordTool,
            "4": this.changeInstrumentTool,
            "5": this.clearProgressionTool
        }
    }
    
    render() {
        return (
            <Tabs defaultActiveKey={1} id="tool-selector" onSelect={(key, event) => {
                this.currentTool = key
                }}>
                <Tab eventKey={1} title="Change Key or Mode">
                    <ProgressionSubTool ops={1} ref={this.keyOrModeTool} progression={this.progression} />
                </Tab>
                <Tab eventKey={2} title="Add a Chord">
                    <ProgressionSubTool ops={2} ref={this.addChordTool} progression={this.progression} />
                </Tab>
                <Tab eventKey={3} title="Modify Chords">
                    <ProgressionSubTool ops={3} ref={this.modifyChordTool} progression={this.progression} />
                </Tab>
                <Tab eventKey={4} title="Clear/Save/Reload">
                    <ProgressionSubTool ops={4} ref={this.clearProgressionTool} progression={this.progression}/>
                </Tab>
            </Tabs>
        )
    }
}
