import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { Form, FormControl ,Button, InputGroup, ButtonGroup, FormLabel, ButtonToolbar } from 'react-bootstrap';
import * as Util from './utils/progressionUtils'
import "./ProgressionSubTool.css"


export default class ProgressionSubTool extends Component {
    constructor(props) {
        super(props)
        this.progression = this.props.progression
        this.optionIdentifier = this.props.ops
        this.mode = null;
        this.key= null;
    }

    renderLeftOps(optionIdentifier) {
        switch (optionIdentifier) {
            case 1: 
                return (
                    <div>
                        <Form className="key-form">
                            <Form.Group id="KeyEntryBox" >
                                <Form.Label className="boldp">Enter Key</Form.Label>
                                <Form.Label className="subpp">(b = flat, # = sharp)</Form.Label>
                                <InputGroup  >
                                    <Form.Control ref="keyInput" placeholder="Enter Key" />
                                    <InputGroup.Append>
                                        <Button display="inline"
                                        id="submitKey" 
                                        variant="primary" 
                                        type="submit"
                                        onClick={e => this.handleChange(e, this.key, this.refs.keyInput)}>Change Key</Button>
                                    </InputGroup.Append>
                                </InputGroup>   
                            </Form.Group>
                            <p className="boldp">Or</p>
                            <Form.Group id="KeySelection">
                                <Form.Label className="boldp">Select key</Form.Label>
                                <FormControl ref="keySelect" as="select"
                                onChange={e => this.handleChange(e, this.key, this.refs.keySelect)}>
                                     {Util.keys.map((keyi, i) =>
                                        <option key={i} value={keyi}>{keyi}</option>)}
                                </FormControl>
                            </Form.Group>
                        </Form>                           
                    </div>
                )
            case 2:
            return (
                <div>
                <Form className="chord-form">
                    <Form.Group id="ChordEntryBox" >
                        <Form.Label className="boldp">Enter Chord</Form.Label>
                        <Form.Label className="subp">(b = flat, # = sharp)</Form.Label>
                        <InputGroup id="chordInput" >
                            <Form.Control placeholder="Enter Chord" ref="chordInput"/>
                        </InputGroup>
                        <Button display="inline" 
                                variant="primary" 
                                type="submit"
                                id="enterChord"
                                onClick={e => this.handleChange(e)}>Submit Chord</Button>
                    </Form.Group>
                </Form>                           
                </div>
            )
            case 3:
            return (
                <div>
                    <Form className="delete-form">
                        <Form.Group id="selectSwapChord">
                            <FormLabel className="boldp">Swap Two Chords by Placement</FormLabel>
                            <FormLabel className="subp">Enter Number separated by /</FormLabel>
                            <FormLabel className="subp">For Example => 1/2</FormLabel>
                            <InputGroup >
                                <Form.Control placeholder="Enter Swap" ref="swapInput"/>
                            </InputGroup>
                            <Button display="inline" 
                                variant="primary" 
                                type="submit"
                                id="chordSwapper"
                                onClick={e => this.handleChange(e)}>
                                Submit Chord
                            </Button>
                        </Form.Group>
                    </Form>
                </div>
            )
            case 4:
            return (
                <div>
                    <ButtonToolbar>
                        <Button 
                        id="clearProgression"
                        onClick= {e => this.handleChange(e)}
                        variant="primary">
                        Clear Progression</Button>
                    </ButtonToolbar>
                </div>
            )
                

                
        }
    }

    renderRightOps(optionIdentifier) {
        switch(optionIdentifier) {
            case 1:
                let selectors = Object.keys(Util.mode_types)
                return (
                    <Form className="mode-form">
                        <Form.Group controlId="ModeEntryBox">
                            <Form.Label className="boldp">Enter Mode</Form.Label>
                            <InputGroup >
                                <Form.Control ref="modeInput" placeholder="Enter Mode"/>
                                <InputGroup.Append>
                                    <Button id="submitMode" 
                                    display="inline" 
                                    variant="primary" 
                                    type="submit"
                                    onClick={e => this.handleChange(e, this.refs.modeInput)}>Change Mode</Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </Form.Group>
                        <p className="boldp">Or</p>
                        <Form.Group id="KeySelection">
                            <Form.Label className="boldp">Select Mode Type</Form.Label>
                            <FormControl ref="modeSelect" id="keySelect" as="select"
                                onChange={e => this.handleChange(e)}>
                                {this.createSelectItems(selectors)}
                            </FormControl>
                         </Form.Group>
                    </Form>
                )
            case 3:
                return (
                    <Form className="modify-buttons">
                        <Form.Group controlId="ModifyChordsBox">
                            <ButtonGroup vertical>
                                <Button 
                                id="removeButton"
                                ref="removeButton" 
                                onClick={e => this.handleChange(e)}>Remove Last Chord</Button>
                            </ButtonGroup>
                        </Form.Group>
                    </Form>
                )
            case 4:
                return (
                <div>
                <ButtonToolbar>
                    <Button 
                    id="saveProgression"
                    onClick= {e => this.handleChange(e)}
                    variant="primary">
                    Save Progression
                    </Button>
                </ButtonToolbar>
                <p>Saving a Progression will clear the stave, hit reload to reload from local</p>
                <ButtonToolbar>
                    <Button 
                    id="reloadProgression"
                    onClick= {e => this.handleChange(e)}
                    variant="primary">
                    Reload Progression
                    </Button>
                </ButtonToolbar>
                </div>
                )
            default:
                break;
        }
    }

    createSelectItems = (selections) => {
        // Found Fix: was not returning any values from this function
        if(selections) {
            return selections.map((value, index) => <option key={index}>{value}</option>)
        }  
    }

    handleChange = (e) =>  {
        switch(e.type){
            case 'click':
                this.handleRef(e) 
                break;
            default:
            console.log("event not captured")
        }
    }

    handleRef = (e) => {
        switch(e.target.id) {
            case "clearProgression":
                this.progression.current.clearEverything()
                break;
            case "chordSwapper":
                var setVar = this.refs.swapInput.value.split("/")
                this.progression.current.swapChords(setVar[0], setVar[1])
                break;
            case "removeButton":
                this.progression.current.removeLast()
                break;
            case "saveProgression":
                this.progression.current.saveProgressionToJSON()
                break;
            case "reloadProgression":
                this.progression.current.reloadProgressionFromJSON()
                break;
            case "submitMode":
                break;
            case "submitKey":
                break;    
            default:
                var setVar = this.refs.chordInput.value
                this.progression.current.addChord(setVar)
        }
    }

    render() {
        return (
            <div className="subtool-display">
                <div className='chromatic-image-window'>PlaceHolder for Graphic</div>
                <div className="progression-options-window">
                    <div className="left-ops">
                        {this.renderLeftOps(this.optionIdentifier)}
                    </div>
                    <div className="right-ops">
                        {this.renderRightOps(this.optionIdentifier)}
                    </div>
                </div>
            </div>
            
        )
    }
}