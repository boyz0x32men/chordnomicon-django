import React, { Component } from "react";
import { Link, withRouter } from "react-router-dom";
import {HashRouter as Router} from "react-router-dom";
import { connect } from "react-redux";
import * as actions from "../../store/actions/auth";

class NavBar extends Component {
  
  render() {
    const loggedIn = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <button onClick={this.props.logout} className="nav-link btn btn-info btn-sm text-light">Log Out</button>
        </li>
      </ul>
    );

    const loggedOut = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Router>
            <Link to="/login" className="nav-link">Log In</Link>
          </Router>
        </li>
        <li className="nav-item">
          <Router>
            <Link to="/register" className="nav-link">Sign Up</Link>
          </Router>
        </li>
      </ul>
    );

    return (
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
        <a className="navbar-brand" href="#home">Chordnomicon</a>
        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav ml-auto m-2">
            <li className="nav-item">
              <Router>
                <Link to="/score-builder" className="nav-link">Score Builder</Link>
              </Router>
            </li>
            <li className="nav-item">
              <Router>
                <Link to="/chord-builder" className="nav-link">Chord Builder</Link>
              </Router>
            </li>
            <li className="nav-item">
              <Router>
                <Link to="/progression-builder" className="nav-link">Progression Builder</Link>
              </Router>
            </li>
            <li className="nav-item">
              <Router>
                <Link to="/mode-builder" className="nav-link">Mode Builder</Link>
              </Router>
            </li>
          </ul>
          { this.props.isAuthenticated ? loggedIn : loggedOut }
        </div>
      </nav>
    );
  }
}

const mapDispatchToProps = dispatch => {
	return {
		logout: () => dispatch(actions.logout())
	};
};

export default withRouter(connect(null, mapDispatchToProps)(NavBar));
