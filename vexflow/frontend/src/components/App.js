import React, { Component, Fragment } from 'react';
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import "./App.css";
import NavBar from "./layouts/NavBar";
import LogIn from "./accounts/LogIn";
import Register from "./accounts/Register";
import ScoreBuilder from './scorebuilder/ScoreBuilder';
import ProgressionBuilder from './progressionbuilder/ProgressionBuilder';
import ChordBuilder from './chordbuilder/ChordBuilder';
import ModeBuilder from './modebuilder/ModeBuilder';
import * as actions from '../store/actions/auth';
import {debug} from '../util/debug';
import HomePage from './home/HomePage';

class App extends Component {
    // constructor(props) {
    //     super(props);
    //     this.state = {};
    // }

    componentDidMount(){
        this.props.onTryAutoSignup();
    }

    render() {
        return (
            <Router>
                <Fragment>
                    <NavBar {...this.props}/>
                    <div className="component">
                        <Switch>
                            <Route exact path="/" component={HomePage} />
                            <Route exact path="/home" component={HomePage} />
                            <Route exact path="/score-builder" component={ScoreBuilder} />
                            <Route exact path="/chord-builder" component={ChordBuilder} />
                            <Route exact path="/progression-builder" component={ProgressionBuilder} />
                            <Route exact path="/mode-builder" component={ModeBuilder} />
                            <Route exact path="/login" component={LogIn} />
                            <Route exact path="/register" component={Register} />
                        </Switch>
                    </div>
                    <link
                        rel="stylesheet"
                        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
                        integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS"
                        crossOrigin="anonymous"
                        />
                </Fragment>
            </Router>
        );
    }
}

const mapStateToProps = state => {
	return {
		isAuthenticated: state.token !== null
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onTryAutoSignup: () => dispatch(actions.checkAuthState())
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
