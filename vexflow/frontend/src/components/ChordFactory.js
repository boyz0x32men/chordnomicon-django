import Vex from 'vexflow';
import {Component} from 'react';
import NoteFactory from './NoteFactory';

const VF = Vex.Flow;

export default class ChordFactory extends Component {
    static getChordByName(name, octave){
        var i = 0;
        var j = 0;
        var needsMore = true;
        var tonic;
        var slashBass;
        var chord = [];

        if(i + 1 <= name.length && this.checkNoteName(name[i])){
            tonic = name[i];
            i++;
        }
        else {return null;}

        if (i + 1 <= name.length && (name[i] === 'b' || name[i] === '#'))
        {
            tonic = tonic + name[i]; 
            i++;
        }
        
        
        chord[j] = tonic + "/" + octave; j++;
        

        // Calculate first interval (5, dim3, m3, M3, aug3)
        if (needsMore)
        {
            // X5
            if (i + 1 <= name.length && name[i] === '5')
            {
                i++;
                
                chord[j] = NoteFactory.getNoteByValue(7, tonic) + "/" + this.getOctave(octave, chord[0], 7); j++;
                
                needsMore = false;
            }
            // Xsus
            else if (i + 3 <= name.length && name[i] === 's' && name[i + 1] === 'u' && name[i + 2] === 's')
            {
                i = i + 3;
                if (i + 1 <= name.length && name[i] === '2')
                {
                    
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 2); j++;
                    i++;
                }
                else { chord[j] = NoteFactory.getNoteByValue(5, tonic) + "/" + this.getOctave(octave, chord[0], 5); j++; }
                
                chord[j] = NoteFactory.getNoteByValue(7, tonic) + "/" + this.getOctave(octave, chord[0], 7); j++;
                needsMore = false;
            }
            // X7sus
            else if (i + 4 <= name.length && name[i] === '7' && name[i + 1] === 's' && name[i + 2] === 'u' && name[i + 3] === 's')
            {
                chord[j] = NoteFactory.getNoteByValue(5, tonic) + "/" + this.getOctave(octave, chord[0], 5); j++;
                chord[j] = NoteFactory.getNoteByValue(7, tonic) + "/" + this.getOctave(octave, chord[0], 7); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                i = i + 4;
                needsMore = false;
            }
            // Xdim
            else if (i + 3 <= name.length && name[i] === 'd' && name[i + 1] === 'i' && name[i + 2] === 'm')
            {
                chord[j] = NoteFactory.getNoteByValue(3, tonic) + "/" + this.getOctave(octave, chord[0], 3); j++;
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                i = i + 3;
                needsMore = false;
            }
            //Xm
            else if (i + 1 === name.length && name[i] === 'm')
            {
                chord[j] = NoteFactory.getNoteByValue(3, tonic) + "/" + this.getOctave(octave, chord[0], 3); j++;
                i++;
            }
            // Xm...
            else if (i + 1 <= name.length && name[i] === 'm' && name[i + 1] != 'a')
            {
                chord[j] = NoteFactory.getNoteByValue(3, tonic) + "/" + this.getOctave(octave, chord[0], 3); j++;
                i++;
            }
            // X...
            else
            {
                chord[j] = NoteFactory.getNoteByValue(4, tonic) + "/" + this.getOctave(octave, chord[0], 4); j++;
            }
        }

        // Add next interval (dim5, P5, aug5)

        if (needsMore)
        {
            // X(b5)
            if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === 'b' && name[i + 2] === '5' 
                && name[i + 3] === ')')
            {
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                i = i + 4;
                needsMore = false;
            }
            // X7(b5)
            else if (i + 5 <= name.length && name[i] === '7' && name[i + 1] === '(' && name[i + 2] === 'b'
                && name[i + 3] === '5' && name[i + 4] === ')')
            {
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                i = i + 5;
                needsMore = false;
            }
            // Xmaj7(b5)
            else if (i + 8 <= name.length && name[i] === 'm' && name[i + 1] === 'a' && name[i + 2] === 'j'
                && name[i + 3] === '7' && name[i + 4] === '(' && name[i + 5] === 'b' && name[i + 6] === '5')
            {
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                i = i + 8;
                needsMore = false;
            }
            // X9(b5)
            else if (i + 5 <= name.length && name[i] === '9' && name[i + 1] === '(' && name[i + 2] === 'b'
                && name[i + 3] === '5')
            {
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                i = i + 5;
                needsMore = false;
            }
            // X13(b9b5)
            else if (i + 8 <= name.length && name[i] === '1' && name[i + 1] === '3' && name[i + 2] === '('
                && name[i + 3] === 'b' && name[i + 4] === '9' && name[i + 5] === 'b' && name[i + 6] === '5')
            {
                chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 6); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                chord[j] = NoteFactory.getNoteByValue(1, tonic) + "/" + this.getOctave(octave, chord[0], 13); j++;
                chord[j] = NoteFactory.getNoteByValue(9, tonic) + "/" + this.getOctave(octave, chord[0], 21); j++;
                i = i + 8;
                needsMore = false;
            }
            // Xaug
            else if (i + 3 <= name.length && name[i] === 'a' && name[i + 1] === 'u' && name[i + 2] === 'g')
            {
                chord[j] = NoteFactory.getNoteByValue(8, tonic) + "/" + this.getOctave(octave, chord[0], 8); j++;
                i = i + 3;
                needsMore = false;
            }
            // X7+
            else if (i + 2 <= name.length && name[i] === '7' && name[i + 1] === '+')
            {
                chord[j] = NoteFactory.getNoteByValue(8, tonic) + "/" + this.getOctave(octave, chord[0], 8); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                i = i + 2;
                // X7+(b9)
                if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === 'b' && name[i + 2] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(1, tonic) + "/" + this.getOctave(octave, chord[0], 13); j++;
                    i = i + 4;
                }
                needsMore = false;
            }
            // X9+
            else if (i + 2 <= name.length && name[i] === '9' && name[i + 1] === '+')
            {
                chord[j] = NoteFactory.getNoteByValue(8, tonic) + "/" + this.getOctave(octave, chord[0], 8); j++;
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                i = i + 2;
                needsMore = false;
            }
            // X...
            else { chord[j] = NoteFactory.getNoteByValue(7, tonic) + "/" + this.getOctave(octave, chord[0], 7); j++; }
        }

        // Add final intervals (M6, m7, M7)

        if (needsMore)
        {
            // X(add9)
            if (i + 6 <= name.length && name[i] === '(' && name[i + 1] === 'a' && name[i + 2] === 'd'
                && name[i + 3] === 'd' && name[i + 4] === '9')
            {
                chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                i = i + 6;
                needsMore = false;
            }
            // X6
            else if (i + 1 <= name.length && name[i] === '6')
            {
                chord[j] = NoteFactory.getNoteByValue(9, tonic) + "/" + this.getOctave(octave, chord[0], 9); j++;
                i++;
                // X6/9
                if (i + 2 <= name.length && name[i] === '/' && name[i + 1] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                    i = i + 2;
                }
                needsMore = false;
            }
            // X13
            else if (i + 2 <= name.length && name[i] === '1' && name[i + 1] === '3')
            {
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                i = i + 2;
                // X13(b9)
                if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === 'b' && name[i + 2] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(1, tonic) + "/" + this.getOctave(octave, chord[0], 13); j++; 
                    i = i + 4;
                }
                // X13(#9)
                else if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === '#' && name[i + 2] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(3, tonic) + "/" + this.getOctave(octave, chord[0], 15); j++;
                    i = i + 4;
                }
                else { chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++; }
                chord[j] = NoteFactory.getNoteByValue(9, tonic) + "/" + this.getOctave(octave, chord[0], 21); j++;
                needsMore = false;
            }
            // X7
            else if (i + 1 <= name.length && name[i] === '7')
            {
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                i++;
                // X7(b9)
                if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === 'b' && name[i + 2] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(1, tonic) + "/" + this.getOctave(octave, chord[0], 13); j++;
                    i = i + 4;
                }
                // X7(#9)
                else if (i + 4 <= name.length && name[i] === '(' && name[i + 1] === '#' && name[i + 2] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(3, tonic) + "/" + this.getOctave(octave, chord[0], 15); j++;
                    i = i + 4;
                }
                needsMore = false;
            }
            // X9
            else if (i + 1 <= name.length && name[i] === '9')
            {
                i++;
                // X9(#11)
                if (i + 5 <= name.length && name[i] === '(' && name[i + 1] === '#' && name[i + 2] === '1'
                    && name[i + 3] === '1')
                {
                    chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                    chord[j] = NoteFactory.getNoteByValue(6, tonic) + "/" + this.getOctave(octave, chord[0], 18); j++;
                    i = i + 5;
                    needsMore = false;
                }
                // X9maj7
                else if (i + 4 <= name.length && name[i] === 'm' && name[i + 1] === 'a'
                && name[i + 2] === 'j' && name[i + 3] === '7')
                {
                    chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                    i = i + 4;
                    needsMore = false;
                }
                // X9(maj7)
                else if (i + 6 <= name.length && name[i] === '(' && name[i + 1] === 'm' && 
                name[i + 2] === 'a' && name[i + 3] === 'j' && name[i + 4] === '7' && name[i + 5] === ')')
                {
                    chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                    i = i + 6;
                    needsMore = false;
                }
                else
                {
                    chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                }
                needsMore = false;
            }
            // X11
            else if (i + 1 <= name.length && name[i] === '1' && name[i + 1] === '1')
            {
                chord[j] = NoteFactory.getNoteByValue(10, tonic) + "/" + this.getOctave(octave, chord[0], 10); j++;
                chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                chord[j] = NoteFactory.getNoteByValue(5, tonic) + "/" + this.getOctave(octave, chord[0], 17); j++;
                i = i + 2;
                needsMore = false;
            }
            // XM11
            else if (i + 3 <= name.length && name[i] === 'M' && name[i + 1] === '1' && name[i + 2] === '1')
            {
                chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                chord[j] = NoteFactory.getNoteByValue(5, tonic) + "/" + this.getOctave(octave, chord[0], 17); j++;
                i = i + 3;
                needsMore = false;
            }
            // Xmaj7
            else if (i + 3 <= name.length && name[i] === 'm' && name[i + 1] === 'a' && name[i + 2] === 'j')
            {
                chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                i = i + 3;
                // Xmaj9
                if (i + 1 <= name.length && name[i] === '9')
                {
                    chord[j] = NoteFactory.getNoteByValue(2, tonic) + "/" + this.getOctave(octave, chord[0], 14); j++;
                }
                i++;
                needsMore = false;
            }
            // X(maj7)
            else if (i + 6 <= name.length && name[i] === '(' && name[i + 1] === 'm' && name[i + 2] === 'a' && name[i + 3] === 'j' 
                && name[i + 4] === '7')
            {
                chord[j] = NoteFactory.getNoteByValue(11, tonic) + "/" + this.getOctave(octave, chord[0], 11); j++;
                i = i + 6;
                needsMore = false;
            }
        }

        // X/bassNote
        if (i < name.length && name[i] === '/')
        {
            i++;
            if (i < name.length && this.checkNoteName(name[i]))
            {
                slashBass = name[i];
                i++;
            }
            else {return null;}

            if (i < name.length && (name[i] === 'b' || name[i] === '#'))
            {
                slashBass = slashBass + name[i];
                i++;
            }

            chord = this.insertNote(slashBass, chord, octave);
        }
        if (i < name.length) { return null }
        
        return chord;
        //return new VF.StaveNote({clef: "treble", keys: chord, duration: duration) });
    }

    static checkNoteName(note){
        switch(note){
            case "A":
            return true;
            case "B":
            return true;
            case "C":
            return true;
            case "D":
            return true;
            case "E":
            return true;
            case "F": 
            return true;
            case "G":
            return true;
            default:
            return false;
        }
    }

    static insertNote(slashBass, chord, octave){
        for(let i = chord.length; i > 0; i--){
            chord[i] = chord[i-1];
        }
        let negativeDistance =  NoteFactory.getNoteValue(slashBass) - NoteFactory.getNoteValue(chord[1][0]);
        if(negativeDistance <= 12){negativeDistance = negativeDistance + 12;}
        negativeDistance = -negativeDistance;
        chord[0] = slashBass + "/" + this.getOctave(octave, chord[1], negativeDistance);
        return chord;
    }

    static getOctave(octave, tonic, distance){
        let returnOctave = octave;
        let rootValue = NoteFactory.getNoteValue(tonic[0]);
        if(distance >= 12){
            returnOctave++;
            distance = distance - 12;
            if(rootValue <= 3){
                if(rootValue + distance >= 4){returnOctave++;}
            }
            else if(rootValue >= 4){
                if(rootValue + distance >= 16){returnOctave++;}
            }
        }
        else if(distance <= 0){
            if(rootValue <= 3){
                if(rootValue - distance >= 4){returnOctave--;}
            }
            else if(rootValue >= 4){
                if(rootValue - distance >= 16){returnOctave--;}
            }
        }
        else{
            if(rootValue <= 3){
                if(rootValue + distance >= 4){returnOctave++;}
            }
            else if(rootValue >= 4){
                if(rootValue + distance >= 16){returnOctave++;}
            }
        }
        return returnOctave;
    }
}