import React, { Component } from 'react';
import Center from 'react-center';

export class HomePage extends Component {
  render() {
    //const logo = favicon;
    return (
        <div>
            <Center>
                <img src={ require('../../../src/logo.ico') } />
            </Center>
            <Center>
                <p>
                    Chordnomicon is a collection of all possible musical chords and their names. 
                </p>
            </Center>
            <Center>
                <p>
                    This web application also allows users 
                    to build chord progressions and displays pictorial aids for the harmonic relationships between chords.
                </p>
            </Center>
        </div>
    )
  }}

export default HomePage
