import Mode from './Mode';
import IntervalFactory from './IntervalFactory';

export default class ModeFactory
    {
        static getModeByName(name)
        {
            let mode = new Mode();
            name = name.toLowerCase()
            //--------------------------------------------------------
            //------------------Heptatonic Modes----------------------
            //--------------------------------------------------------

            //------------------Golden Modal Shape---------------------
            if (name === "lydian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Lydian");
            }
            else if (name === "ionian" || name === "major")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "ionian") { mode.setName("Ionian"); }
                else { mode.setName("Major"); }
            }
            else if (name === "mixolydian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Mixolydian");
            }
            else if (name === "dorian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Dorian");
            }
            else if (name === "aeolian" || name === "minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                if (name === "aeolian") { mode.setName("Aeolian"); }
                else { mode.setName("Minor"); }
            }
            else if (name === "phrygian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Phrygian");
            }
            else if (name === "locrian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Locrian");
            }

            //------------------Silver Modal Shape---------------------
            else if (name === "lydian augmented")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Lydian Augmented");
            }
            else if (name === "lydian flat-seven" || name === "lydian flat seven" || 
            name === "lydian flat 7" || name === "lydian b7" || name === "acoustic")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                if (name === "acoustic") { mode.setName("Acoustic"); }
                else { mode.setName("Lydian Flat-Seven"); }
            }
            else if (name === "half diminished")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Half Diminished");
            }
            else if (name === "harmonic mixolydian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Harmonic Mixolydian");
            }
            else if (name === "altered")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Altered");
            }
            else if (name === "melodic minor" || name === "jazz melodic minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "melodic minor") { mode.setName("Melodic Minor"); }
                else { mode.setName("Jazz Melodic Minor"); }
            }
            else if (name === "harmonic phrygian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Harmonic Phrygian");
            }

            //------------------Bronze Modal Shape---------------------
            else if (name === "super augmented")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Super Augmented*");
            }
            else if (name === "mixolydian augmented")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Mixolydian Augmented");
            }
            else if (name ===  "minor lydian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Minor Lydian");
            }
            else if (name === "major locrian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Major Locrian");
            }
            else if (name === "aeolian diminished")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Aeolian Diminished");
            }
            else if (name === "super diminished")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Super Diminished*");
            }
            else if (name === "neapolitan major" || name === "neapolitan")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "neapolitan") { mode.setName("Neapolitan"); }
                else { mode.setName("Neapolitan Major"); }
            }

            //------------------Other Heptatonic Modal Shapes---------------------
            else if (name === "hungarian minor" || name === "gypsy")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "gypsy") { mode.setName("Gypsy"); }
                else { mode.setName("Hungarian Minor"); }
            }
            /*
            else if (name === "diminished dorian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Diminished Dorian*");
            }
            else if (name === "dominant augmented")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Dominant Augmented*");
            }
            else if (name === "melodic locrian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Melodic Locrian*");
            }
            */
            else if (name === "double harmonic" || name === "flamenco")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "flamenco") { mode.setName("Flamenco"); }
                else { mode.setName("Double Harmonic"); }
            }
            /*
            else if (name === "augmented mixolydian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Augmented Mixolydian*");
            }
            else if (name === "harmonic locrian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim7"));
                mode.setName("Harmonic Locrian*");
            }
            */
            else if (name === "harmonic minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Harmonic Minor");
            }
            else if (name === "ukrainian dorian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Ukrainian Dorian");
            }
            else if (name === "phrygian dominant")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Phrygian Dominant");
            }
            else if (name === "harmonic major")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Harmonic Major");
            }
            else if (name === "pfluke")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Pfluke");
            }
            else if (name === "neapolitan minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Neapolitan Minor");
            }
            else if (name === "enigmatic")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Enigmatic");
            }
            else if (name === "persian")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Persian");
            }
            else if (name === "blues with flat-four" || name === "blues with flat four" || name === "blues with b4")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Blues with Flat-Four");
            }
            //--------------------------------------------------------
            //------------------Pentatonic Modes----------------------
            //--------------------------------------------------------

            //------------------Phi Modal Shape----=------------------
            else if (name === "pentatonic major")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.setName("Pentatonic Major");
            }
            else if (name === "pentatonic minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Pentatonic Minor");
            }
            else if (name === "suspended")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Suspended");
            }
            else if (name === "blues major")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.setName("Blues Major");
            }
            else if (name === "blues minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Blues Minor");
            }
            //------------------Other Pentatonic Modes----------------------
            else if (name === "hirajoshi")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Hirajoshi");
            }
            else if (name === "in")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.setName("In");
            }
            else if (name === "iwato")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Iwato");
            }
            else if (name === "insen")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Insen");
            }

            //--------------------------------------------------------
            //------------------Hexatonic Modes-----------------------
            //--------------------------------------------------------
            
            //-------------------Blues Modal Shape-------------------
            else if (name === "blues")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Blues");
            }
            /*
            else if (name === "harmonic blues")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.setName("Harmonic Blues");
            }
            else if (name === "dominant blues minor")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug5"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Dominant Blues Minor");
            }
            else if (name === "nores blues")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Nores Blues");
            }
            else if (name ===  "dominant blues major")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Dominant Blues Major");
            }
            else if (name === "melodic blues")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Melodic Blues");
            }
            */
            //--------------Symmetric Hexatonic Modal Shapes----------
            else if (name === "whole tone" || name === "whole step")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                if (name === "whole step") { mode.setName("Whole Step"); }
                else { mode.setName("Whole Tone"); }
            }
            else if (name === "augmented" || name === "hexatonic augmented")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("aug2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                if (name === "augmented") { mode.setName("Augmented"); }
                else { mode.setName("Hexatonic Augmented"); }
            }
            else if (name === "hexatonic diminished")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim7"));
                mode.setName("Hexatonic Diminished");
            }

            //--------------Other Hexatonic Modal Shapes----------
            else if (name === "tritone")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Tritone");
            }
            else if (name === "prometheus")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Prometheus");
            }
            else if (name === "istran")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim6"));
                mode.setName("Istran");
            }
            //--------------------------------------------------------
            //------------------Octatonic Modes----------------------
            //--------------------------------------------------------

            //--------------Symmetric Octatonic Modal Shape------------
            else if (name === "diminished whole-half" || name === "diminished whole half")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Diminished Whole-Half");
            }
            else if (name === "diminished half-whole" || name === "diminished half whole")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.setName("Diminished Half-Whole");
            }

            //--------------Other Octatonic Modal Shapes------------
            else if (name === "major bebop")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Major Bebop");
            }
            else if (name === "bebop dominant")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Bebop Dominant");
            }
            else if (name === "chromatic")
            {
                mode.addInterval(IntervalFactory.getIntervalByName("R"));
                mode.addInterval(IntervalFactory.getIntervalByName("m2"));
                mode.addInterval(IntervalFactory.getIntervalByName("M2"));
                mode.addInterval(IntervalFactory.getIntervalByName("m3"));
                mode.addInterval(IntervalFactory.getIntervalByName("M3"));
                mode.addInterval(IntervalFactory.getIntervalByName("P4"));
                mode.addInterval(IntervalFactory.getIntervalByName("dim5"));
                mode.addInterval(IntervalFactory.getIntervalByName("P5"));
                mode.addInterval(IntervalFactory.getIntervalByName("m6"));
                mode.addInterval(IntervalFactory.getIntervalByName("M6"));
                mode.addInterval(IntervalFactory.getIntervalByName("m7"));
                mode.addInterval(IntervalFactory.getIntervalByName("M7"));
                mode.setName("Chromatic");
            }
            else { return null; }
            return mode;
        }
    }