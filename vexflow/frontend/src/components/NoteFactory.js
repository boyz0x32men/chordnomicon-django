export default class NoteFactory{
    static getNoteByValue(distance, key){
        while(distance > 12) {distance = distance - 12;}
        while(distance < 0) {distance = distance + 12;}
        
        let _note;
        let keyValue = this.getNoteValue(key);
        let noteValue = keyValue + distance;
        while(noteValue > 12) {noteValue = noteValue - 12;}
        
        if (noteValue === 1) { _note = "A"; }
        else if (noteValue === 2)
        {
            if (key.length === 2)
            {
                if (key[1] === '#') { _note = "A#"; }
                else if (key[1] === 'b') { _note = "Bb"; }
            }
            else if (distance === 2 || distance === 4 || distance === 7 || distance === 9 || distance === 11)
            { _note = "A#"; }
            else { _note = "Bb"; }
        }
        else if (noteValue === 3)
        {
            if (key === "Bb") { _note = "Cb"; }
            else if (key === "Gb") { _note = "Cb"; }
            else { _note = "B"; }
        }
        else if (noteValue === 4)
        {
            if (key === "C#") { _note = "B#"; }
            else { _note = "C"; }
        }
        else if (noteValue === 5)
        {
            if (distance === 0) { _note = key; }
            else if (key.length === 2)
            {
                if (key[1] === '#') { _note = "C#"; }
                else if (key[1] === 'b') { _note = "Db";  }
            }
            else if (distance === 2 || distance === 4 || distance === 7 || distance === 9 || distance === 11)
            { _note = "C#";  }
            else { _note = "Db";  }
        }
        else if (noteValue === 6) { _note = "D";  }
        else if (noteValue === 7)
        {
            if (distance === 0) { _note = key; }
            else if (key.length === 2)
            {
                if (key[1] === '#') { _note = "D#";  }
                else if (key[1] === 'b') { _note = "Eb";  }
            }
            else if (distance === 2 || distance === 4 || distance === 7 || distance === 9 || distance === 11)
            { _note = "D#";  }
            else { _note = "Eb";  }
        }
        else if (noteValue === 8)
        {
            if (key === "Eb") { _note = "Fb";  }
            else if (key === "Cb") { _note = "Fb";  }
            else { _note = "E";  }
        }
        else if (noteValue === 9)
        {
            if (key === "F#") { _note = "E#";  }
            else if (key === "C#") { _note = "E#";  }
            else { _note = "F";  }
        }
        else if (noteValue === 10)
        {
            if (distance === 0) { _note = key; }
            else if (key.length === 2)
            {
                if (key[1] === '#') { _note = "F#";  }
                else if (key[1] === 'b') { _note = "Gb";  }
            }
            else if (distance === 2 || distance === 4 || distance === 7 || distance === 9 || distance === 11)
            { _note = "F#";  }
            else { _note = "Gb";  }
        }
        else if (noteValue === 11) { _note = "G";  }
        else if (noteValue === 12)
        {
            if (distance === 0) { _note = key; }
            else if (key.length === 2)
            {
                if (key[1] === '#') { _note = "G#"; }
                else if (key[1] === 'b') { _note = "Ab"; }
            }
            else if (distance === 2 || distance === 4 || distance === 7 || distance === 9 || distance === 11)
            { _note = "G#"; }
            else { _note = "Ab"; }
        }
        
        return _note;
    }
    
    static getNoteValue(tonic){
        switch(tonic){
            case "A":
            return 1;
            case "A#":
            return 2;
            case "Bb":
            return 2;
            case "B":
            return 3;
            case "Cb":
            return 3;
            case "B#":
            return 4;
            case "C":
            return 4;
            case "C#":
            return 5;
            case "Db":
            return 5;
            case "D":
            return 6;
            case "D#":
            return 7;
            case "Eb":
            return 7;
            case "E":
            return 8;
            case "Fb":
            return 8;
            case "E#":
            return 9;
            case "F":
            return 9;
            case "F#":
            return 10;
            case "Gb":
            return 10;
            case "G":
            return 11;
            case "G#":
            return 12;
            case "Ab":
            return 12;
            default:
            return null;
        }
    }
}