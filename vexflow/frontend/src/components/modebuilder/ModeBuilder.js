import React, { Component } from 'react';
import Center from 'react-center';

export class ModeBuilder extends Component {
  render() {
    return (
        <div>
            <Center>
                <img src={ require('../../../src/logo.ico') } />
            </Center>
            <Center>
                <p>
                    The mode builder is coming soon!
                </p>
            </Center>
        </div>
    )
  }
}

export default ModeBuilder
