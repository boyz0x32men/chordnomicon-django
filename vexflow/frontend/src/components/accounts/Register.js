import React from 'react';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom';
import * as actions from '../../store/actions/auth';
import { Form, Spin, Icon, Input, Button } from 'antd';

const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

class Register extends React.Component {

    state = {
        confirmDirty: false
    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                this.props.onAuth(
                    values.username,
                    values.email, 
                    values.password,
                    values.confirm,
                );
            }
        });
        this.props.history.push('/');
    }

    confirmPasswords = (rule, value, callback) => {
        const form = this.props.form;
        if (value && value !== form.getFieldValue('password')) {
            callback('Your passwords must match!');
        } else {
            callback();
        }
    }
    
    validateToNextPassword = (rule, value, callback) => {
        const form = this.props.form;
        if (value && this.state.confirmDirty) {
            form.validateFields(['confirm'], { force: true });
        }
        callback();
    }
    
    handleConfirmBlur = (e) => {
        const value = e.target.value;
        this.setState({ confirmDirty: this.state.confirmDirty || !!value });
    }

    render() {
        let errorMessage = null;
        if (this.props.error) {
            errorMessage = (
                <p>{this.props.error.message}</p>
            );
        }
        const { getFieldDecorator } = this.props.form;
        return (
            <div>
                {errorMessage}
                {
                    this.props.loading ?
                    <Spin indicator={antIcon} />
                    :
                    <Form onSubmit={this.handleSubmit} className="register-form">
                        <Form.Item>
                            {getFieldDecorator('username', {
                                rules: [{
                                    required: true, message: 'Please input your username!',
                                }],
                            })(
                                <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Username" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('email', {
                                rules: [{
                                    required: true, message: 'Please input your email!',
                                }],
                            })(
                                <Input prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />} placeholder="Email" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            {getFieldDecorator('password', {
                                rules: [{
                                required: true, message: 'Please input your password!',
                                }, {
                                validator: this.validateToNextPassword,
                                }],
                            })(
                                <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Password" />
                            )}
                        </Form.Item>
                        {/* <li>Password must be at least 12 characters</li>   */}
                        <Form.Item>
                            {getFieldDecorator('confirm', {
                                rules: [{
                                required: true, message: 'Please confirm your password!',
                                }, {
                                validator: this.confirmPasswords,
                                }],
                            })(
                                <Input onBlur={this.handleConfirmBlur} prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" placeholder="Confirm Password" />
                            )}
                        </Form.Item>
                        <Form.Item>
                            <p>
                                <Button type="primary" htmlType="submit" style={{marginRight: '10px'}}>Signup</Button>
                            </p>
                            <p>
                                Already have an account? 
                                <NavLink style={{marginRight: '10px'}} to="/login">Login</NavLink>
                            </p>
                        </Form.Item>
                    </Form>
                }
            </div>
        );
    }
}

const WrappedAuth = Form.create()(Register);

const mapStateToProps = state => {
    return {
        loading: state.loading,
        error: state.error
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
        onAuth: (username, email, password1, password2) => dispatch(actions.register(username, email, password1, password2))
    }
  }
  
  export default connect(mapStateToProps, mapDispatchToProps)(WrappedAuth);
