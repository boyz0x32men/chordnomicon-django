import Vex from 'vexflow';
import * as VexUtils from '../../util/VexUtils';
import * as NoteUtils from './utils/NoteUtils';
import * as AccidentalTool from './utils/NoteTools/AccidentalTool';
import {Component} from 'react';
import {debug} from '../../util/debug'

const VF = Vex.Flow;

export default class Note extends Component {
	constructor(props){
        super(props);
        this.context = this.props.context;
        this.note_json = this.props.note;
        this.clef = this.props.clef;
        this.type = null;
        this.duration = null;
        this.accidentals = [];
        this.beamcheck = null;
        this.voice = null;
        this.pitch  = null;
        this.temp_props = {
			pitch: null,
			modifiers: null,
			duration: null
		};
        this.vexNote = null;
        this.isRest = null;
		this.score = this.props.score;
		this.init();
	}

	init() {
        this.parseProperties();

        if (this.isRest) {
            this.pitch = "b/4"
            if (!this.type) {
                this.type = "wr"
            } else {
                this.type = this.type + "r"
            }
        }

        if (debug) { console.log("NOTE:", this.type, this.pitch, this.clef) }
	}

    parseProperties() {
        for (let property of this.note_json.content) {
            // console.log("Note Property: " + property.name.key);
            switch(property.name.key){
                case "duration":
                    // Duration doesn't actually seem like a useable property from musixml to vexflow - use type instead
                    // this.duration = property.value;
                    break;
                case "voice":
                    if (debug) { console.log ("Unhandled note property:", property.name.key); }
                    break;
                case "type":
                    this.type = VexUtils.getType(property.value.value);
                    // console.log("Note Type: " + this.type);
                    break;
                case "stem":
                    if (debug) { console.log ("Unhandled note property:", property.name.key); }
                    break;
                case "notations":
                    if (debug) { console.log ("Unhandled note property:", property.name.key); }
                    break;
                case "dot":
                    if (debug) { console.log ("Unhandled note property:", property.name.key); }
                    break;
                case "rest":
                    this.isRest = true;
                    break;
                case "pitch":
                    this.pitch = property.value.step.toLowerCase() + "/" + property.value.octave;
                    // console.log("Note Pitch: " + this.pitch);
                    break;
                case "beam":
                    // sets the beamcheck value to (begin/continue/end)
                    if(this.beamcheck === null) {
                        this.beamcheck = property.value.value
                    }
                    break;
                case "lyric":
                    //TODO: Render lyrics at some point
                    if (debug) { console.log ("Unhandled tab property:", property.name.key); }
                    break;
                case "tie":
                    // TODO: Figure out Stave ties, both inside measure and across measure
                    if (debug) { console.log ("Unhandled tab property:", property.name.key); }
                    break;
                case "accidental":
					var accidentalName = property.value.value
					for (var key in AccidentalTool.toolList) {
						if (AccidentalTool.toolList[key].hasOwnProperty("mxmlName")) {
							if (AccidentalTool.toolList[key]["mxmlName"] === accidentalName) {
								this.accidentals.push( new VF.Accidental(key))
							}
						}
					}
                    break;
                case "staff":
                    // TODO: Handle, I think this prop is used for piano since they commonly have two staves
                    if (debug) { console.log ("Unhandled tab property:", property.name.key); }
                    break;
                case "chord":
                    // TODO: signifies that note is part of chord, if applicable always comes before pitch
                    if (debug) { console.log ("Unhandled tab property:", property.name.key); }
                    break;
                case "grace":
                    // TODO: implement grace notes
                    if (debug) { console.log ("Unhandled tab property:", property.name.key); }
                    break;
                default:
                    if(debug) { console.log ("Unknown note property:", property.name.key); }
            }
        }
    }

    createVexNote() {
        if (this.clef && this.pitch && this.type) {
            this.vexNote = new VF.StaveNote({
                clef: this.clef,
				keys: [this.pitch],
				duration: this.type
			})
		}
		if (this.vexNote){
			for (var i = 0; i< this.accidentals.length; i++) {
				this.vexNote.addModifier(i, this.accidentals[i]);
			}
		}
	}

    createCustomBoundingBox(stave) {
        let boundingBox = this.vexNote.getBoundingBox()
        if(boundingBox) {
            let rectAttributes = {
                'fill': 'tomato',
                'fill-opacity': 0,
                'x': boundingBox.x,
                'y': stave.y + 5, //offset for visual reasons
                'width': boundingBox.w,
                'height': 120 // This is based on allowing 3 ledger lines
            }

            var rectangle = document.createElementNS('http://www.w3.org/2000/svg', 'rect');
            this.context.applyAttributes(rectangle, rectAttributes)

            // open/crete SVG element group, add customBoundingBox, add as attribute to note
            this.vexNote.setAttribute('bb', this.context.openGroup('custombounding', "bb-"+ this.vexNote.getAttribute('id'), { pointerBBox: true}))
            this.context.add(rectangle);
            this.context.closeGroup()
        }
	}

    createEventListeners(){
        let customBoundingBox = this.vexNote.getAttribute('bb')
        if(customBoundingBox) {
            customBoundingBox.addEventListener("dblclick", (event) => {
				this.mouseEventHandler(event, this)
            }, false);
            customBoundingBox.addEventListener("mouseenter", (event) => {
				this.mouseEventHandler(event, this)
            }, false);
			customBoundingBox.addEventListener("mousemove", (event) => {
				this.mouseEventHandler(event, this)
			}, false);
			customBoundingBox.addEventListener("mouseout", (event) => {
				this.mouseEventHandler(event, this)
			}, false);
			customBoundingBox.addEventListener("click", (event) => {
				this.mouseEventHandler(event, this)
			}, false);
        }
    }

    removeNoteFromDOM() {
        // "el" is the note's DOM element, 'bb' is the custom bounding box we added
        this.vexNote.getAttribute('el').remove()
        // note.getAttribute('bb').remove()
    }

    mouseEventHandler(event, note) {
		switch (note.score.tool.current.currentTool) {
			case "1":
				if (!note.isRest) {
					NoteUtils.pitchTool(event, note);
				}
                break;
            case "2":
				if (!note.isRest) {
					NoteUtils.accidentalTool(event, note);
				}
                break;
            case "3":
                NoteUtils.durationTool(event, note);
                break;
            default:
                break;
		}
    }

    updateNoteJSON() {
		// console.log(this.note_json.content)

        for(let property of this.note_json.content) {
            switch(property.name.key){
                case "pitch":
                    var updated = this.vexNote.keys[0].split('/')
                    property.value.step = updated[0]
                    property.value.octave = updated[1]
                    break;
                case "type":
					property.value.value = VexUtils.getDurationFromType(this.vexNote.duration)
                    break;
                case "accidental":
					for (var key in AccidentalTool.toolList) {
						if (AccidentalTool.toolList[key].code === this.vexNote.modifiers[0].accidental.code) {
							property.value.value = AccidentalTool.toolList[key].mxmlName
							break;
						}
					}
                    break;
                default:
                    if(debug) { console.log ("Unknown note property:", property.name.key); }
            }
        }
    }
}
