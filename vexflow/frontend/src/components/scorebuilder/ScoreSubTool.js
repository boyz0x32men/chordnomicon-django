import React, { Component } from 'react';
import {ToggleButton, ToggleButtonGroup, TabContent} from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";
import ScoreSubToolIcon from './ScoreSubToolIcon';

export default class ScoreSubTool extends Component {
	constructor(props){
		super(props);
		this.tool = this.props.tool;
		this.selection = 0;
	}

	setSelection = (selection) =>{
		return this.selection = selection;
	}

	render() {
			return 	(
				<TabContent>
					<ToggleButtonGroup defaultValue={0} defaulttype='radio' name='option-set' onChange={this.setSelection}>
						{this.tool.utils.toolListArray.map((subtoolSelection, i) =>
							<ToggleButton key={i} value={i} className="subToolButton">
								<ScoreSubToolIcon index={i} tool={this.tool}/>
							</ToggleButton>)
						}
					</ToggleButtonGroup>
				</TabContent>
			)
	}
}
