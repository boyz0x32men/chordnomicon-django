// import Vex from 'vexflow';
import Measure from './Measure'
import {Component} from 'react';

export default class Part extends Component {
	constructor(props) {
		super(props);
		this.score = this.props.score;
		this.score_width = this.props.score_width;
		this.width_measures = 0;
		this.stave_offset = this.props.stave_offset;
		this.stave_height = 0;
		this.part_json = this.props.part;
		this.context = this.props.context;
		this.measures = [];
		this.staves = [];
		this.init();
	}

	init() {
		this.parseMeasures();
	}

	parseMeasures (){
		var currentAttributes;
		for (let measure of this.part_json.measure) {
			// console.log("Measure: " + measure.TYPE_NAME);
			var newMeasure = new Measure({
				"measure": measure,
				"context": this.context,
				"score": this.score
			});

			if (newMeasure.noAttributes()) {
				newMeasure.attributes = currentAttributes;
			} else {
				currentAttributes = newMeasure.attributes;
			}

			// console.log("Attributes for this measure:", currentAttributes)

			if (newMeasure.attributes.clef === "tab") {
				newMeasure.parseTabNotes();
			} else {
				newMeasure.parseNotes();
			}

			newMeasure.createVexNotes();
			if (this.width_measures + newMeasure.width >= this.score_width) {
				this.width_measures = 0;
				this.stave_height += this.stave_offset;
			}

			newMeasure.createStave(this.width_measures, this.stave_height);
			newMeasure.drawBeams();
			newMeasure.addInteractionToNotes();

			this.width_measures += newMeasure.stave.width;

			this.measures.push(newMeasure);
		}
	}
}
