import * as PitchTool from './NoteTools/PitchTool';
import * as AccidentalTool from './NoteTools/AccidentalTool';
import * as DurationTool from './NoteTools/DurationTool';

export function pitchTool(event, note) {
	switch (event.type) {
		case "mousemove":
			PitchTool.mouseMove(event, note);
			break;
		case "click":
			PitchTool.click(event, note);
			break;
		case "mouseout":
			PitchTool.mouseOut(event, note);
			break;
		default:
			break;
	}
}

export function accidentalTool(event, note) {
	switch(event.type) {
		case "click":
			AccidentalTool.click(note);
			break;
		case "mouseout":
			AccidentalTool.mouseOut(note)
			break;
		case "dblclick":
			AccidentalTool.doubleClick(note);
			break;
		case "mouseenter":
			AccidentalTool.mouseEnter(note);
			break;
		default:
			break;
	}
}

export function durationTool(event, note) {
	switch (event.type) {
		case "mouseenter":
			DurationTool.mouseEnter(note);
			break;
		case "click":
			DurationTool.click(note);
			break;
		case "mouseout":
			DurationTool.mouseOut(note);
			break;
		default:
			break;
	}
}
