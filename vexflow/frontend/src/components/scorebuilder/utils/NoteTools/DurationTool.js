import Vex from 'vexflow';

const VF = Vex.Flow;

export function mouseEnter(note) {
	// Sets temp preview accidental until click or mouseout
	note.temp_props.duration = note.vexNote.duration;
	note.vexNote.setStyle({fillStyle: "grey", strokeStyle: "grey"});
	note.vexNote.setLedgerLineStyle({fillStyle: "lightgrey", strokeStyle: "lightgrey"});
	// add duration based on tool selection
	note.vexNote.duration = getDuration(note.score.tool.current.getSubTool());
	reRenderNote(note);
}

export function click(note) {
	// Save selected duration to note
	note.vexNote.duration = getDuration(note.score.tool.current.getSubTool());
	note.temp_props.duration = null
	note.vexNote.setStyle({fillStyle: "black", strokeStyle: "black"});
	note.vexNote.setLedgerLineStyle({fillStyle: "black", strokeStyle: "black"})
	reRenderNote(note)
	note.updateNoteJSON()
}

export function mouseOut(note) {
	// Return to saved version of note on mouseOut
	renderStoredDuration(note)
}

function renderStoredDuration (note) {
	if (note.temp_props.duration) {
		note.vexNote.duration = note.temp_props.duration
		note.temp_props.duration = null
	}
	note.vexNote.setStyle({fillStyle: "black", strokeStyle: "black"});
	note.vexNote.setLedgerLineStyle({fillStyle: "black", strokeStyle: "black"})
	reRenderNote(note)
}

function reRenderNote (note) {
	note.removeNoteFromDOM();

	// Re-initialize note
	note.vexNote.glyph = VF.durationToGlyph(note.vexNote.duration, note.vexNote.noteType);
	note.vexNote.keyProps = [];
	note.vexNote.note_heads = [];
	note.vexNote.preFormat();
	note.vexNote.calculateKeyProps();
	note.vexNote.buildStem()
	//TODO: Fix Flags when changing to lower order notes, currently the flags remain if present before
	note.vexNote.buildFlag();
	note.vexNote.autoStem();
	note.vexNote.reset();

	note.vexNote.setStave(note.vexNote.getStave())

	note.vexNote.draw()

	// replace custom Bounding Box on top of new note
	note.context.svg.appendChild(note.vexNote.getAttribute("bb"))
}

export function renderGlyphIcon (index, ctx) {
	var note = toolList[toolListArray[index]]

	ctx.fillStyle = "white";
	ctx.strokeStyle = "white";

	var g = new Vex.Flow.Glyph(note.noteHead, 40);
	g.render(ctx, 10, 30);

	if(note.stem) {
		ctx.beginPath();
		ctx.moveTo(22, 30);
		ctx.lineTo(22, 5);
		ctx.stroke();
	}
	if(note.flag) {
		g = new Vex.Flow.Glyph(note.flag, 25);
		g.render(ctx, 22, 0);
	}

	ctx.save();
}

// Based on vexflow/blob/master/src/tables.js: Flow.durationToTicks.durations
export var toolList = {
  '1/2': {
	  duration: VF.RESOLUTION * 2,
	  noteHead: "v53",
	  stem: null,
	  flag: null
  },
  '1': {
	  duration: VF.RESOLUTION / 1,
	  noteHead: "v1d",
	  stem: null,
	  flag: null
  },
  '2': {
	  duration: VF.RESOLUTION / 2,
	  noteHead: "v81",
	  stem: true,
	  flag: null
  },
  '4': {
	  duration: VF.RESOLUTION / 4,
	  noteHead: "vb",
	  stem: true,
	  flag: null
  },
  '8': {
	  duration: VF.RESOLUTION / 8,
	  noteHead: "vb",
	  stem: true,
	  flag: "v54"
  },
  '16': {
	  duration: VF.RESOLUTION / 16,
	  noteHead: "vb",
	  stem: true,
	  flag: "v3f"
  },
  '32': {
	  duration: VF.RESOLUTION / 32,
	  noteHead: "vb",
	  stem: true,
	  flag: "v47"
  },
  '64': {
	  duration: VF.RESOLUTION / 64,
	  noteHead: "vb",
	  stem: true,
	  flag: "va9"
  },
  // '128': {
	//   duration: VF.RESOLUTION / 128,
	//   noteHead: "vb",
	//   stem: true,
	//   flag: "v9b"
  // },
  // '256': {
	//   duration: VF.RESOLUTION / 256,
	//   glyph: ["vb"]
  // }
};

export var toolListArray = Object.keys(toolList)

export function getDuration(index) {
	return toolListArray[index]
}
