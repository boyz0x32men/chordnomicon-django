import * as VexUtils from '../../../../util/VexUtils';

export function mouseMove(event, note) {
	// Get local BB coordinates
	var svg = note.context.svg
	var svgPt = svg.createSVGPoint()
	svgPt.x = event.clientX;
	svgPt.y = event.clientY;
	const svgCoords = svgPt.matrixTransform(svg.getScreenCTM().inverse());
	// console.log( "local coords:", svgCoords.x-event.target.attributes.x.value , svgCoords.y-event.target.attributes.y.value)
	var height = svgCoords.y-event.target.attributes.y.value

	note.temp_props.pitch = VexUtils.getPitchFromY(height)

	if (note.pitch !== note.temp_props.pitch){
		note.vexNote.setStyle({fillStyle: "grey", strokeStyle: "grey"});
		note.vexNote.setLedgerLineStyle({fillStyle: "lightgrey", strokeStyle: "lightgrey"})

		renderNewPitch(note, note.temp_props.pitch)
	}
}

export function click(event, note) {
	if (note.vexNote.keys[0] !== note.pitch) {
		note.pitch = note.temp_props.pitch
		note.keys = [note.pitch]
	}
	renderStoredPitch(note);
	note.updateNoteJSON()
}

export function mouseOut(event, note) {
	// When mousing out, render last pitch known to note (ditch temp notes)
	renderStoredPitch(note);
}

function removeBoundingBoxLedgers (note) {
	// get x value of bounding box for note, and ledger offset depending on bb width
	let boundingBox = note.vexNote.getAttribute("bb").childNodes[0]
	let bbX = boundingBox.x.baseVal.value + getXOffset(boundingBox.width.baseVal.value)

	bbX = parseInt('' + (bbX * 100)) / 100

	// find all ledger lines from this bounding box and remove them
	// (go through array backwards so removing doesn't disturb contents)
	var nodes = note.context.svg.childNodes
	for (let i = nodes.length-1; i>-1; i--) {
		if (nodes[i].nodeName === "path"){
			if (nodes[i].attributes.d.nodeValue.startsWith("M" + bbX)) {
				nodes[i].remove()
			}
		}
	}
}

function getXOffset (BBWidth) {
	if (BBWidth < 24.05) {
		return -3
	} else if (BBWidth < 34.59) { // width of notes with naturals
		return 7.53904
	} else if (BBWidth < 35.49) { // width of notes with flats
		return 8.44192
	} else if (BBWidth < 36.88) { // width of notes with sharps
		return 9.83728
	} else {
		return 0
	}
}

function renderStoredPitch (note) {
	removeBoundingBoxLedgers(note)

	note.vexNote.setStyle({fillStyle: "black", strokeStyle: "black"});
	note.vexNote.setLedgerLineStyle({fillStyle: "black", strokeStyle: "black"})
	renderNewPitch(note, note.pitch)
	note.temp_props.pitch = null;
}

function renderNewPitch (note, newPitch) {
	note.removeNoteFromDOM();

	// Re-initialize note with pitch change
	note.vexNote.keys = [newPitch]
	note.vexNote.keyProps = [];
	note.vexNote.note_heads = [];
	// note.vexNote.modifiers = []; // clears accidentals
	note.vexNote.calculateKeyProps();
	note.vexNote.buildStem();
	note.vexNote.autoStem();
	note.vexNote.reset();
	note.vexNote.buildFlag();
	note.vexNote.setStave(note.vexNote.getStave())

	note.vexNote.draw()

	// replace custom Bounding Box on top of new note
	note.context.svg.appendChild(note.vexNote.getAttribute("bb"))
}
