import Vex from 'vexflow'

const VF = Vex.Flow;

//global index var for accidentals
var index = 0;

export function mouseEnter(note) {
	// Sets temp preview accidental until click or mouseout
	note.temp_props.modifiers = note.vexNote.modifiers;
	note.vexNote.setStyle({fillStyle: "grey", strokeStyle: "grey"});
	note.vexNote.setLedgerLineStyle({fillStyle: "lightgrey", strokeStyle: "lightgrey"});
	// add accidental based on tool selection
	note.vexNote.modifiers = []
	const accident = createAccidental(note.score.tool.current.getSubTool())
	note.vexNote.addModifier(index, accident);
	reRenderNote(note);
}

export function click(note) {
	// Save selected accidental to note
	const accident = createAccidental(note.score.tool.current.getSubTool())
	if(!note.vexNote.modifiers[index]){
		note.vexNote.addModifier(index, accident);
	}
	note.temp_props.modifiers = null
	note.vexNote.setStyle({fillStyle: "black", strokeStyle: "black"});
	note.vexNote.setLedgerLineStyle({fillStyle: "black", strokeStyle: "black"})
	reRenderNote(note)
	note.updateNoteJSON()
}

export function doubleClick(note) {
	// Removes all accidentals from note
	removeAccidentals(note)
}

export function mouseOut(note) {
	// Return to saved version of note on mouseOut
	renderStoredAccidental(note)
}

function renderStoredAccidental (note) {
	if (note.temp_props.modifiers) {
		note.vexNote.modifiers = note.temp_props.modifiers
		note.temp_props.modifiers = null
	}
	note.vexNote.setStyle({fillStyle: "black", strokeStyle: "black"});
	note.vexNote.setLedgerLineStyle({fillStyle: "black", strokeStyle: "black"})
	reRenderNote(note)
}

function removeAccidentals(note) {
	note.vexNote.modifiers = []
	reRenderNote(note)
}

function reRenderNote (note) {
	note.removeNoteFromDOM();

	// Re-initialize note
	note.vexNote.keyProps = [];
	note.vexNote.note_heads = [];
	note.vexNote.preFormat();
	note.vexNote.calculateKeyProps();
	note.vexNote.buildStem()
	//TODO: Fix Flags when changing to lower order notes, currently the flags remain if present before
	note.vexNote.buildFlag();
	note.vexNote.autoStem();
	note.vexNote.reset();

	note.vexNote.setStave(note.vexNote.getStave())

	note.vexNote.draw()

	// replace custom Bounding Box on top of new note
	note.context.svg.appendChild(note.vexNote.getAttribute("bb"))
}

export function renderGlyphIcon (index, ctx) {
	var glyph = toolList[toolListArray[index]].code;
	var g = new Vex.Flow.Glyph(glyph, 40);
	ctx.fillStyle = "white";
	g.render(ctx, 15, 20);
	ctx.save();
}

// Based on vexflow/blob/master/src/tables.js: Flow.accidentalCodes.accidentals
export var toolList = {
  '#': { code: 'v18', parenRightPaddingAdjustment: -1, mxmlName: 'sharp' },
  '##': { code: 'v7f', parenRightPaddingAdjustment: -1, mxmlName: 'double-sharp' },
  'b': { code: 'v44', parenRightPaddingAdjustment: -2, mxmlName: 'flat' },
  'bb': { code: 'v26', parenRightPaddingAdjustment: -2, mxmlName: 'flat-flat' },
  'n': { code: 'v4e', parenRightPaddingAdjustment: -1, mxmlName: 'natural' },
  // '{': { code: 'v9c', parenRightPaddingAdjustment: -1 },   // Parens, which should be a single modifier for other accidentals, not individual tools
  // '}': { code: 'v84', parenRightPaddingAdjustment: -1 },
  'db': { code: 'v9e', parenRightPaddingAdjustment: -1 },
  'd': { code: 'vab', parenRightPaddingAdjustment: 0 },
  'bbs': { code: 'v90', parenRightPaddingAdjustment: -1 },
  '++': { code: 'v51', parenRightPaddingAdjustment: -1 },
  '+': { code: 'v78', parenRightPaddingAdjustment: -1 },
  '+-': { code: 'v8d', parenRightPaddingAdjustment: -1 },
  '++-': { code: 'v7a', parenRightPaddingAdjustment: -1 },
  'bs': { code: 'vb7', parenRightPaddingAdjustment: -1 },
  'bss': { code: 'v39', parenRightPaddingAdjustment: -1 },
  'o': { code: 'vd0', parenRightPaddingAdjustment: -1 },
  'k': { code: 'vd1', parenRightPaddingAdjustment: -1 },
}

export function createAccidental(index){
	return new VF.Accidental(toolListArray[index])
}

export var toolListArray = Object.keys(toolList)
