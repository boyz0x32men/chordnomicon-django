import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from 'react-bootstrap/Button';

import {debug} from '../../util/debug'
import "./ScoreBuilder.css";
import testObject from './musicjson/testObject.json';
import helloWorld from './musicjson/helloWorld.json';
import accidentalObject from './musicjson/accidentalObject.json'
import echigo from './musicjson/echigo-jishi.json'
import ScoreTool from './ScoreTool';
import Score from './Score';

export default class ScoreBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score: "none",
            width: window.innerWidth - 100,
        };
        this.tool = React.createRef()
    }

    componentDidMount() {
        window.addEventListener('resize', this.windowResized)
    }

    windowResized = () => {
        if (debug) console.log('window.innerWidth:', window.innerWidth);
        this.setState({ width: (window.innerWidth - 100) });
    }

        swapScore = () => {
            if (debug) console.log("Swapping score from:", this.state.score);
            switch (this.state.score) {
                case "helloWorld":
                    this.setState({ score: "testObject" });
                    break;
                case "testObject":
                    this.setState({ score: "accidentalObject" });
                    break;
                case "accidentalObject":
                    this.setState({ score: "echigo"});
                    break;
                case "echigo":
                    this.setState({ score: "undefined"});
                    break;
                default:
                    this.setState({ score: "helloWorld" });
            }
        }

        getScore = () => {
            // console.log("Getting Score:", this.state.score)
            switch (this.state.score) {
                case "helloWorld":
                    return helloWorld;
                case "testObject":
                    return testObject;
                case "accidentalObject":
                    return accidentalObject;
                case "echigo":
                    return echigo;
                default:
                    return "undefined";
            }
        }

        getScoreName = () => {
            switch (this.state.score) {
                case "helloWorld":
                    return "Hello World Demo";
                case "testObject":
                    return "West Point Demo";
                case "accidentalObject":
                    return "An die ferne Geliebte Demo"
                case "echigo":
                    return "Echigo Demo"
                default:
                    return "Chordnomicon Demo";
            }
        }


	render() {
        return (
            <div className="App">
                <div className="AppBar">
                    <AppBar position="static" color="inherit">
                        <Toolbar className="Toolbar">
                            <Button variant="primary" id="btn1" onClick={this.swapScore}>Swap</Button>
                            <div className="padded-tool">{this.getScoreName()}</div>
                        </Toolbar>
                    </AppBar>
                </div>
                <div className="ToolBar">
                    <ScoreTool ref={this.tool}/>
                </div>
                <header className="App-header">
                    <Score id="scoreObject" score={this.getScore()} width={this.state.width} tool={this.tool} />
                </header>
            </div>
        );
    }
}
