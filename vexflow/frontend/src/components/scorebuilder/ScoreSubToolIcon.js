import React, { Component } from 'react';

export default class ScoreSubToolIcon extends Component {
	constructor(props){
		super(props);
		this.canvas = React.createRef();
		this.index = this.props.index;
        this.tool = this.props.tool;
	}

	setSelection = (selection) =>{
		return this.selection = selection;
	}

	componentDidMount() {
		this.canvasFunction ()
	}

	canvasFunction () {
		if (this.canvas.current){
			var canvas = this.canvas.current
			var ctx = canvas.getContext("2d");
			this.tool.utils.renderGlyphIcon(this.index, ctx)
		}
	}

	render() {
			return 	(
                <canvas width="40" height="40" ref={this.canvas} className="subToolButtonIcon"></canvas>
			)
	}
}
