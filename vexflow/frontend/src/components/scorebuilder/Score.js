import Vex from 'vexflow';
import './ScoreBuilder.css'
import Part from './Part'
import React, { Component } from 'react';
import {debug} from '../../util/debug'
// https://github.com/0xfe/vexflow/issues/338

const {
    // Accidental,
    // Formatter,
    // Stave,
    // StaveNote,
    Renderer,
} = Vex.Flow;

export default class Score extends Component {
	constructor(props) {
		super(props);
		this.score_json = this.props.score;
		this.context = null;
		this.renderer = null;
		this.width = this.props.width;
		this.height = 0;
		this.parts = [];
		this.totalWidth = 0;
		this.time_signature = "4/4";
        this.tool = this.props.tool;
	}

    componentDidMount() {
        if (debug) console.log("Score componentDidMount")
        this.createScore()
    }

    componentDidUpdate() {
        if (debug) console.log("Score componentDidUpdate")
        this.score_json = this.props.score;
        this.width = this.props.width
        this.createScore()
    }

	clear() {
		this.refs.outer.innerHTML = '';
	}

    createScore() {
        this.clear();
        if (this.score_json !== "undefined"){
            this.initializeVex();
            this.parseParts();
        }
    }

    initializeVex() {
        this.renderer = new Renderer(this.refs.outer, Renderer.Backends.SVG);
        this.context = this.renderer.getContext();
    }

    parseParts() {
        this.parts = []
        this.height = 0;
        for (let part of this.score_json.value.part) {
            // console.log("Part: " + part.TYPE_NAME);
            var newPart = new Part({
                "score_width": this.width,
                "part": part,
                "context": this.context,
                "score": this,
                "stave_offset": this.score_json.value.part.length * 100
            });
            this.parts.push(newPart);

            // Find max part height and use its height to render
            if (newPart.stave_height + newPart.stave_offset > this.height) {
                this.height = newPart.stave_height + newPart.stave_offset
        }
    }
        // Resize renderer once we have determined height
        this.renderer.resize(this.width, this.height);
    }

	render() {
        return (
            <div className="Score-display" ref="outer"></div>
        );
    }
}
