import Vex from 'vexflow';
import {Component} from 'react';
import {debug} from '../../util/debug'

const VF = Vex.Flow;

export default class TabNote extends Component {
	constructor(props){
		super(props);
		this.note_json = this.props.note;
        this.clef = this.props.clef;
        this.type = null;
        this.duration = null;
        this.voice = null;
        this.pitch  = null;
		this.positions = [];
        this.vexNote = null;
		this.score = this.props.score;
		this.init();
	}

	init() {
        this.parseProperties();
        if (debug) { console.log("NOTE:", this.type, this.pitch, this.clef) }
	}

    parseProperties() {
        for (let property of this.note_json.content) {
			switch(property.name.key){
				case "duration":
					// Duration doesn't actually seem like a useable property from musixml to vexflow
					this.duration = property.value;
					// console.log ("Note Duration: " + this.duration);
					break;
				case "voice":
					if (debug) { console.log ("Unhandled tab property:", property.name.key); }
					break;
				case "type":
					this.type = this.getType(property.value.value);
					// console.log("Note Type: " + this.type);
					break;
				case "stem":
					if (debug) { console.log ("Unhandled tab property:", property.name.key); }
					break;
				case "notations":
					let string;
					let fret;
					for (let stringFret of property.value.tiedOrSlurOrTuplet[0].upBowOrDownBowOrHarmonic) {
						if (stringFret.name.key === "string") {
							string = stringFret.value.value;
						} else if (stringFret.name.key === "fret") {
							fret = stringFret.value.value;
						}
					}
					this.positions.push({
						"str": string,
						"fret": fret
					})
					break;
				case "dot":
					if (debug) { console.log ("Unhandled tab property:", property.name.key); }
					break;
				case "pitch":
					this.pitch = property.value.step.toLowerCase() + "/" + property.value.octave;
					// console.log("Note Pitch: " + this.pitch);
					break;
				default:
					if (debug) { console.log ("Unknown tab property:", property.name.key); }
			}
        }
    }

    getType (type) {
        switch (type) {
            case "whole":
            return "w";

            case "half":
            return "h";

            case "quarter":
            return "q";

            case "eighth":
            return "8";

            case "16th":
            return "16";

            case "32nd":
            return "32";

            case "64th":
            return "64";

            default:
            return null;
        }
    }

    createVexNote () {
		this.vexNote = new VF.TabNote({
		    positions: this.positions,
		    duration: this.type
		});
    }
}
