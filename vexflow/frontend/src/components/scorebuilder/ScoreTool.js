import React, { Component } from 'react';
import {Tabs, Tab} from 'react-bootstrap';
import "bootstrap/dist/css/bootstrap.css";
import ScoreSubTool from './ScoreSubTool';
import * as AccidentalTool from './utils/NoteTools/AccidentalTool';
import * as DurationTool from './utils/NoteTools/DurationTool';

export default class ScoreTool extends Component {
	constructor(props){
		super(props);
		this.currentTool = "1";
		this.score = this.props.score;
		this.tools = {
		   accidentals: {
			   ref: React.createRef(),
			   utils: AccidentalTool
		   },
		   durations: {
			   ref: React.createRef(),
			   utils: DurationTool
		   }
	   }
	   this.toolMap = {
		   "2": this.tools.accidentals.ref,
		   "3": this.tools.durations.ref
	   }
	}

	getSubTool () {
		return this.toolMap[this.currentTool].current.selection;
	}

	render() {
		return (
			<Tabs defaultActiveKey={1} id="tool-selector" onSelect={(key, event) => {
				this.currentTool = key;
				}}>
				<Tab eventKey={1} title="Pitch Tool">
				</Tab>
				<Tab eventKey={2} title="Accidental Tool">
					<ScoreSubTool tool={this.tools.accidentals} ref={this.tools.accidentals.ref} />
				</Tab>
				<Tab eventKey={3} title="Duration Tool">
					<ScoreSubTool tool={this.tools.durations} ref={this.tools.durations.ref} />
				</Tab>
			</Tabs>
		);
	}
}
