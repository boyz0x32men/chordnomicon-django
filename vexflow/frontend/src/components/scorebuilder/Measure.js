import Vex from 'vexflow';
import Note from './Note';
import TabNote from './TabNote';
import { Component } from 'react';
import {debug} from '../../util/debug'

const VF = Vex.Flow;

export default class Measure extends Component {
    constructor(props) {
        super(props);
        this.edit_state = false;
        this.measure_json = this.props.measure;
        this.context = this.props.context;
        this.index = this.measure_json.number;
        this.score = this.props.score;
        this.beam_groups = [];
        this.beams = [];
        this.group = [];
        this.notes = [];
        this.vexNotes = [];
        this.attributes = {
            "clef": null,
            "time_signature": null
        }
        this.annotation = null;
        this.stave = null;
        this.width = null;
        this.init();
    }

    init() {
        this.checkForAttributes();
        // TODO: find a dynamically vexflow-determined width to use as default instead of 100 (formatter?)
        // probably even use it in place of measure_json.width too since it seems to arbitrarily assigned in mxml
        this.width = this.measure_json.width ? this.measure_json.width : 100;
    }

    checkForAttributes() {
        for (let note of this.measure_json.noteOrBackupOrForward) {
            // console.log("Note: " + note.TYPE_NAME);
            if (note.TYPE_NAME === "XM.Attributes") {
                this.getAttributes(note);
                // console.log ("Attributes: " + JSON.stringify(this.attributes));
                return;
            }
        }
    }

    parseNotes() {
        for (let note of this.measure_json.noteOrBackupOrForward) {
            switch(note.TYPE_NAME) {
                case "XM.Harmony":
                    // harmony chord text listed above staff
                    this.annotation = this.getHarmony(note);
                    // console.log ("Annotation: " + JSON.stringify(this.annotation));
                    break;
                case "XM.Note":
                    var newNote = new Note({
                        "context": this.context,
                        "note": note,
                        "clef": this.attributes.clef,
                        "score": this.score
                    });
                    this.notes.push(newNote);
                    break;
                case "XM.Print":
                    // TODO: Figure out lyrics
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                    break;
                case "XM.Attributes":
                    // TODO: Figure out if usable
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                    break;
                case "XM.Sound":
                    // TODO: Figure out if usable, probably for midi playback
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                    break;
                case ("XM.Backup"):
                    // From the schema doc {The backup and dorward elements are required to cvoordinate multp0lie voices in one part}... whatever that means
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                    break;
                case ("XM.Forward"):
                    // From the schema doc {The backup and dorward elements are required to cvoordinate multp0lie voices in one part}... whatever that means
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                break;
                case "XM.Direction":
                    if (debug) {
                        console.log("Unhandled XML note type:", note.TYPE_NAME)
                    }
                    // TODO: implement for modifiers e.g. crescendo, pedal, diminuendo, etc...
                    break;
                default:
                    // catchall for unknown note types
                    if (debug) {
                        console.log("Unknown XML note type:", note.TYPE_NAME)
                    }
            }
        }
    }

    parseTabNotes() {
        for (let note of this.measure_json.noteOrBackupOrForward) {
            // console.log("Note: " + note.TYPE_NAME);
            if (note.TYPE_NAME === "XM.Note") {
                if (note.content[0].name.string === "chord") {
                    this.fetchTabChord(note.content);
                } else {
                    var newNote = new TabNote({
                        "note": note,
                        "clef": this.attributes.clef,
                        "score": this.score
                    });
                    this.notes.push(newNote);
                }
            } else {
                // catchall for unknown note types
                if (debug) { console.log("Unknown XML tab note type:", note.TYPE_NAME) }
            }
        }
    }

    fetchTabChord(note) {
        for (let property of note) {
            if (property.name.key === "notations") {
                let string;
                let fret;
                for (let stringFret of property.value.tiedOrSlurOrTuplet[0].upBowOrDownBowOrHarmonic) {
                    if (stringFret.name.key === "string") {
                        string = stringFret.value.value;
                    } else if (stringFret.name.key === "fret") {
                        fret = stringFret.value.value;
                    }
                }
                this.notes[this.notes.length - 1].positions.push(
                    {
                        "str": string,
                        "fret": fret
                    }
                )
            }
        }
    }

    createVexNotes() {
        for (let note of this.notes) {
            note.createVexNote();
            this.vexNotes.push(note.vexNote);
        }
    }

    drawBeams() {
        for(let beam of this.beams) {
            beam.draw()
        }
    }

    createBeams() {
        for(let note of this.notes) {
            if(note.duration >2) {
                switch(note.beamcheck) {
                    case "begin":
                        this.group.push(note.vexNote);
                        break;
                    case "continue":
                        this.group.push(note.vexNote);
                        break;
                    case "end":
                        this.group.push(note.vexNote);
                        this.beam_groups.push(this.group);
                        this.group = [];
                        break;
                    default:
                        break;
                }
            }
        }
        for(let group of this.beam_groups) {
            var beam = new VF.Beam(group);
            beam.setContext(this.context)
            this.beams.push(beam)
        }
    }

    createStave(x_position, y_position) {
        if (this.attributes.clef === "tab") {
            this.stave = new VF.TabStave(x_position, y_position+60, this.width);
        } else {
            this.stave = new VF.Stave(x_position, y_position, this.width);
        }

        if (this.index === "1") {
            this.stave.addClef(this.attributes.clef);
            if (this.attributes.time_signature) {
                this.stave.addTimeSignature(this.attributes.time_signature);
            }
        }
        this.createBeams()
        this.stave.setContext(this.context);
        this.stave.draw();
        VF.Formatter.FormatAndDraw(this.context, this.stave, this.vexNotes, {autobeam: true});
    }

    addInteractionToNotes() {
        // still have to iterate through the list of notes because of formatter error
        for (let note of this.notes) {
            if (note.vexNote instanceof VF.StaveNote) {  //checks to make sure operating on notes, not tabnotes as these functions aren't in that class yet
                note.createCustomBoundingBox(this.stave)
                note.createEventListeners()
            }
        }
    }

    getAttributes(note) {
        if (note.clef) {
            this.getClef(note.clef[0]);
        }
        if (note.time) {
            this.getTimeSignature(note.time[0]);
        }
    }

    getClef(clef) {
        try {
            switch (clef.sign) {
                case "C":
                    this.attributes.clef = "alto";
                    break;
                case "F":
                    this.attributes.clef = "bass";
                    break;
                case "TAB":
                    this.attributes.clef = "tab";
                    break;
                default:
                    this.attributes.clef = "treble";
            }
        }
        catch (error) {
            console.log("Malformed XML Clef");
        }
    }

    getTimeSignature(time) {
        try {
            this.attributes.time_signature = time.timeSignature[0].value + "/" + time.timeSignature[1].value;
        }
        catch (error) {
            console.log("Malformed XML time signature");
        }
    }

    getHarmony(harmony) {
        return {
            "fontSize": harmony.fontSize,
            "text": harmony.harmonyChord[0].rootStep.value +
                harmony.harmonyChord[1].text
        };
    }

    noAttributes() {
        return !(this.attributes.clef || this.attributes.time_signature);
    }
}
