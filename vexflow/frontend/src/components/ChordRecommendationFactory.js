import Vex from 'vexflow';
import {Component} from 'react';
import NoteFactory from './NoteFactory'
import ChordFactory from './ChordFactory'

const VF = Vex.Flow;

export default class RecommendedChordFactory
{
    static getFirstRecommendedDegrees (key, mode)
    {
        let notes = [];
        let noteOffset = 0;
        let i = 0

        notes[i] = key;
        i++;

        if (mode.containsIntervalValue(7))
        {
            notes[i] = NoteFactory.getNoteByValue(7, key);
            i++;
        }
        if (mode.containsIntervalValue(5))
        {
            notes[i] = NoteFactory.getNoteByValue(5, key);
            i++;
        }
        if (mode.containsIntervalValue(8))
        {
            notes[i] = NoteFactory.getNoteByValue(8, key);
            i++;
        }
        if (mode.containsIntervalValue(9))
        {
            notes[i] = NoteFactory.getNoteByValue(9, key);
            i++;
        }
        if (mode.containsIntervalValue(3))
        {
            notes[i] = NoteFactory.getNoteByValue(3, key);
            i++;
        }
        if (mode.containsIntervalValue(4))
        {
            notes[i] = NoteFactory.getNoteByValue(4, key);
            i++;
        }
        if (mode.containsIntervalValue(1))
        {
            notes[i] = NoteFactory.getNoteByValue(1, key);
            i++;
        }
        if (mode.containsIntervalValue(2))
        {
            notes[i] = NoteFactory.getNoteByValue(2, key);
            i++;
        }
        if (mode.containsIntervalValue(10))
        {
            notes[i] = NoteFactory.getNoteByValue(10, key);
            i++;
        }
        if (mode.containsIntervalValue(11))
        {
            notes[i] = NoteFactory.getNoteByValue(11, key);
            i++;
        }
        if (mode.containsIntervalValue(6))
        {
            notes[i] = NoteFactory.getNoteByValue(6, key);
            i++;
        }
        return notes;
    }

    static getRecommendedDegreesByLast(key, lastTonic, mode)
    {
        let i = 0;
        let notes = [];
        let noteOffset;
        let distance = NoteFactory.getNoteValue(lastTonic) - NoteFactory.getNoteValue(key);
        while (distance < 12) { distance = distance + 12; }
        
        if (mode.containsIntervalValue(5 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 5;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(2 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 2;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(1 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 1;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(4 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 4;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(3 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 3;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(7 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 7;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(10 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 10;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(11 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 11;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(8 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 8;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(9 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 9;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }
        if (mode.containsIntervalValue(6 + distance))
        {
            noteOffset = NoteFactory.getNoteValue(lastTonic) + 6;
            noteOffset = noteOffset - NoteFactory.getNoteValue(key);
            notes[i] = NoteFactory.getNoteByValue(noteOffset, key);
            i++;
        }

        notes[i] = lastTonic;
        i++;

        return notes;
    }
    static getChordsByDegree (key, tonic, mode, duration)
    {
        let i = NoteFactory.getNoteValue(tonic) - NoteFactory.getNoteValue(key);
        while (i < 0) { i = i + 12; }
        let chords = [];

        // Triads and five diad
        // "X" major 0 4 7 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic, duration); }

        // "Xm" minor 0 3 7
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m", duration); }

        // "Xdim" diminished 0 3 6
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(6 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "dim", duration); }

        // "Xaug" augmented 0 4 8
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(8 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "aug", duration); }

        // "X(b5)" flat five 0 4 6
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(6 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "(b5)", duration); }

        // "X5" five diad 0 7
        if ((mode.containsIntervalValue(7 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "5", duration); }

        // "Xsus" suspended 0 5 7
        if ((mode.containsIntervalValue(5 + i))
        && (mode.containsIntervalValue(7 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "sus", duration); }

        // "Xsus2" suspended two 0 2 7
        if ((mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(7 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "sus2", duration); }

        // Sevens
        // "X7" seven 0 4 7 10
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7", duration); }

        // "Xm7" minor seven 0 3 7 10
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m7", duration); }

        // "X7sus" suspended seven 0 5 7 10
        if ((mode.containsIntervalValue(5 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7sus", duration); }

        // "X7(b5)" suspended seven 0 4 6 10
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(6 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7(b5)", duration); }

        // "Xm7(b5)" suspended seven 0 3 6 10
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(6 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m7(b5)", duration); }

        // "Xmaj7" major seven 0 4 7 11
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(11 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "maj7", duration); }

        // "Xm(maj7)" minor with major seven 0 3 7 11
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(11 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m(maj7)", duration); }

        // "Xmaj7(b5)" major seven flat five 0 4 6 11
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(6 + i))
        && (mode.containsIntervalValue(11 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "maj7(b5)", duration); }

        // "X7+" augmented seven 0 4 8 10
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(8 + i))
        && (mode.containsIntervalValue(10 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7+", duration); }

        // "X7+(b9)" augmented seven 0 4 8 10 1
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(8 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(1 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7+(b9)", duration); }

        // Sixes
        // "X6" seven 0 4 7 9 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "6", duration); }

        // "X6/9" seven 0 4 7 9 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(9 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "6/9", duration); }

        // "Xm6" seven 0 3 7 9
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m6", duration); }

        // "Xm6/9" seven 0 3 7 9 2
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(9 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m6/9", duration); }

        // Nines
        // "X(add9)" add nine 0 4 7 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "(add9)", duration); }

        // "X9" nine 0 4 7 10 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "9", duration); }

        // "Xmaj9" major nine 0 4 7 11 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(11 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "maj9", duration); }

        // "Xm9(maj7)" minor nine with major seven 0 3 7 11 2
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(11 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m9(maj7)", duration); }

        // "X7(b9)" seven flat nine 0 4 7 10 1
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7(b9)", duration); }

        // "X7(#9)" major seven flat five 0 4 7 10 3
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(3 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "7(#9)", duration); }

        // "X9(#11)" nine with sharp eleven 0 4 7 10 2 6
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(6 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "9(#11)", duration); }

        // "X9+" augmented nine 0 4 8 10 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(8 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "9+", duration); }

        // "X9(b5)" nine flat five 0 4 6 10 2
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(6 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "9(b5)", duration); }

        // Elevens
        // "X11" eleven 0 4 7 10 2 5 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(5 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "11", duration); }

        // "Xm11" minor eleven 0 3 7 10 2 5 
        if ((mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(5 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "m11", duration); }

        // "XM11" major eleven 0 4 7 11 2 5 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(11 + i))
        && (mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(5 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "M11", duration); }

        // Thirteens
        // "X13" thirteen 0 4 7 10 2 9 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(2 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "13", duration); }

        // "X13(b9)" thirteen flat nine 0 4 7 10 1 9 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(1 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "13(b9)", duration); }

        // "X13(#9)" thirteen sharp nine 0 4 7 10 3 9 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(7 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(3 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "13(#9)", duration); }

        // "X13(b9b5)" thirteen 0 4 6 10 1 9 
        if ((mode.containsIntervalValue(4 + i))
        && (mode.containsIntervalValue(6 + i))
        && (mode.containsIntervalValue(10 + i))
        && (mode.containsIntervalValue(1 + i))
        && (mode.containsIntervalValue(9 + i)))
        { chords[chords.length] = ChordFactory.getChordByName(tonic + "13(b9b5)", duration); }

        return chords;
    }
}