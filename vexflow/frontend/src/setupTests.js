import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import Vex from 'vexflow';

configure({ adapter: new Adapter() });


// mock for canvas.getContext
window.HTMLCanvasElement.prototype.getContext = function () {
    return {
        fillRect: function() {},
        clearRect: function(){},
        getImageData: function(x, y, w, h) {
            return  {
                data: new Array(w*h*4)
            };
        },
        putImageData: function() {},
        createImageData: function(){ return []},
        setTransform: function(){},
        drawImage: function(){},
        save: function(){},
        fillText: function(){},
        restore: function(){},
        beginPath: function(){},
        moveTo: function(){},
        lineTo: function(){},
        closePath: function(){},
        stroke: function(){},
        translate: function(){},
        scale: function(){},
        rotate: function(){},
        arc: function(){},
        fill: function(){},
        measureText: function(){
            return { width: 0 };
        },
        transform: function(){},
        rect: function(){},
        clip: function(){},
    };
}

Vex.Flow.Glyph= jest.fn().mockImplementation(() => { return {render: function(){}} })
