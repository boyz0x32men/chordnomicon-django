export function getPitchFromY(height) {
	switch(true){
		case height <= 2.5:
			return "e/6"
		case height <= 7.5:
			return "d/6"
		case height < 12.5:
			return "c/6";
		case height < 17.5:
			return "b/5";
		case height < 22.5:
			return "a/5";
		 case height < 27.5:
			return "g/5";
		 case height < 32.5:
			return "f/5";
		case height < 37.5:
			return "e/5";
		case height < 42.5:
			return "d/5";
		case height < 47.5:
			return "c/5";
		 case height < 52.5:
			return "b/4";
		case height < 57.5:
			return "a/4";
		case height < 62.5:
			return "g/4";
		case height < 67.5:
			return "f/4";
		case height < 72.5:
			return "e/4";
		case height < 77.5:
			return "d/4";
		 case height < 85.5:
			return "c/4";
		case height <= 90.5:
			return "b/3";
		case height <= 95.5:
			return "a/3";
		case height <= 100.5:
			return "g/3";
		case height <= 105.5:
			return "f/3"
		 default:
			return "f/3";
	}
}

export function getType (type) {
	switch (type) {
		 case "whole":
		 return "1";

		 case "half":
		 return "2";

		 case "quarter":
		 return "4";

		 case "eighth":
		 return "8";

		 case "16th":
		 return "16";

		 case "32nd":
		 return "32";

		 case "64th":
		 return "64";

		 default:
		 return null;
	}
}

export function getDurationFromType (type) {
	switch (type) {
		 case "1":
		 return "whole";

		 case "2":
		 return "half";

		 case "4":
		 return "quarter";

		 case "8":
		 return "eighth";

		 case "16":
		 return "16th";

		 case "32":
		 return "32nd";

		 case "64":
		 return "64th";

		 default:
		 return null;
	}
}
