from django.apps import AppConfig


class VexflowDemoConfig(AppConfig):
    name = 'backend'
