from django.db import models
from django.contrib.auth.models import User

class ChordnomiconEntity(models.Model):    
    name = models.TextField()    	
    value = models.TextField()
    is_visible = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Mode(ChordnomiconEntity):
    user_id = models.ForeignKey(
        to=User,
        on_delete=models.PROTECT,
        to_field='id'
    )


class Chord(ChordnomiconEntity):
    user_id = models.ForeignKey(
        to=User,
        on_delete=models.PROTECT,
        to_field='id'
    )


class Progression(ChordnomiconEntity):
    user_id = models.ForeignKey(
        to=User,
        on_delete=models.PROTECT,
        to_field='id'
    )


class Score(ChordnomiconEntity):
    user_id = models.ForeignKey(
        to=User,
        on_delete=models.PROTECT,
        to_field='id'
    )


