from backend.api import *
from rest_framework import routers

router = routers.DefaultRouter()
router.register('api/chords', ChordViewSet, 'chords')
router.register('api/modes', ModeViewSet, 'modes')
router.register('api/scores', ScoreViewSet, 'score')
router.register('api/progressions', ProgressionViewSet, 'progressions')

urlpatterns = router.urls
