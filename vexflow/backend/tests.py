from django.test import TestCase
from .models import *
from random import *
import string, pdb

def generate_name():
	name = ''.join(choice(
		string.ascii_letters + 
		string.punctuation + 
		string.digits) for i in range(16))
		
	return name + '@gmail.com'

class ModeModelTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		username = generate_name()
		User.objects.create(
			username=username, 
			password='some password'
		)

		Mode.objects.create(
			user_id=User.objects.get(id=1),
			name='first mode', 
			value='some value'
		)

	def test_get_user_id(self):
		expected_id = Mode.objects.get(id=1).id

		self.assertEqual(expected_id, 1)

	def test_set_user_id(self):
		pass

	def test_get_name(self):
		expected_name = Mode.objects.get(id=1).name

		self.assertEqual(expected_name, 'first mode')

	def test_set_name(self):
		mode = Mode.objects.get(id=1)
		mode.name = 'new name'
		mode.save()

		expected_name = Mode.objects.get(id=1).name

		self.assertEqual(expected_name, 'new name')

	def test_get_value(self):
		expected_value = Mode.objects.get(id=1).value

		self.assertEqual(expected_value, 'some value')

	def test_set_value(self):
		mode = Mode.objects.get(id=1)
		mode.value = 'new value'
		mode.save()

		expected_value = Mode.objects.get(id=1).value

		self.assertEqual(expected_value, 'new value')

	def test_get_is_visible(self):
		expected_visibility = Mode.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, True)

	def test_set_is_visible(self):
		mode = Mode.objects.get(id=1)
		mode.is_visible = False
		mode.save()

		expected_visibility = Mode.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, False)


class ChordModelTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		username = generate_name()
		User.objects.create(
			username=username, 
			password='some password'
		)

		Chord.objects.create(
			user_id=User.objects.get(id=1),
			name='first chord', 
			value='some notes'
		)

	def test_get_user_id(self):
		expected_id = Chord.objects.get(id=1).id

		self.assertEqual(expected_id, 1)

	def test_set_user_id(self):
		pass
		
	def test_get_name(self):
		expected_name = Chord.objects.get(id=1).name
		
		self.assertEqual(expected_name, 'first chord')

	def test_set_name(self):
		chord = Chord.objects.get(id=1)
		chord.name = 'new name'
		chord.save()

		expected_name = Chord.objects.get(id=1).name

		self.assertEqual(expected_name, 'new name')

	def test_get_value(self):
		expected_value = Chord.objects.get(id=1).value

		self.assertEqual(expected_value, 'some notes')

	def test_set_value(self):
		chord = Chord.objects.get(id=1)
		chord.value = 'new value'
		chord.save()

		expected_value = Chord.objects.get(id=1).value

		self.assertEqual(expected_value, 'new value')

	def test_get_is_visible(self):
		expected_visibility = Chord.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, True)

	def test_set_is_visible(self):
		chord = Chord.objects.get(id=1)
		chord.is_visible = False
		chord.save()

		expected_visibility = Chord.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, False)


class ProgressionModelTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		username = generate_name()
		User.objects.create(
			username=username, 
			password='some password'
		)

		Progression.objects.create(
			user_id=User.objects.get(id=1),
			name='first progression', 
			value='some value'
		)

	def test_get_user_id(self):
		expected_id = Progression.objects.get(id=1).id

		self.assertEqual(expected_id, 1)

	def test_set_user_id(self):
		pass

	def test_get_name(self):
		expected_name = Progression.objects.get(id=1).name

		self.assertEqual(expected_name, 'first progression')

	def test_set_name(self):
		progression = Progression.objects.get(id=1)
		progression.name = 'new name'
		progression.save()

		expected_name = Progression.objects.get(id=1).name

		self.assertEqual(expected_name, 'new name')

	def test_get_value(self):
		expected_value = Progression.objects.get(id=1).value

		self.assertEqual(expected_value, 'some value')

	def test_set_value(self):
		progression = Progression.objects.get(id=1)
		progression.value = 'new value'
		progression.save()

		expected_value = Progression.objects.get(id=1).value

		self.assertEqual(expected_value, 'new value')

	def test_get_is_visible(self):
		expected_visibility = Progression.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, True)

	def test_set_is_visible(self):
		progression = Progression.objects.get(id=1)
		progression.is_visible = False
		progression.save()

		expected_visibility = Progression.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, False)


class ScoreModelTest(TestCase):
	
	@classmethod
	def setUpTestData(cls):
		username = generate_name()
		User.objects.create(
			username=username, 
			password='some password'
		)

		Score.objects.create(
			user_id=User.objects.get(id=1),
			name='first score', 
			value='some value'
		)

	def test_get_user_id(self):
		expected_id = Score.objects.get(id=1).id

		self.assertEqual(expected_id, 1)

	def test_set_user_id(self):
		pass

	def test_get_name(self):
		expected_name = Score.objects.get(id=1).name

		self.assertEqual(expected_name, 'first score')

	def test_set_name(self):
		score = Score.objects.get(id=1)
		score.name = 'new name'
		score.save()

		expected_name = Score.objects.get(id=1).name

		self.assertEqual(expected_name, 'new name')

	def test_get_value(self):
		expected_value = Score.objects.get(id=1).value

		self.assertEqual(expected_value, 'some value')

	def test_set_value(self):
		score = Score.objects.get(id=1)
		score.value = 'new value'
		score.save()

		expected_value = Score.objects.get(id=1).value

		self.assertEqual(expected_value, 'new value')

	def test_get_is_visible(self):
		expected_visibility = Score.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, True)

	def test_set_is_visible(self):
		score = Score.objects.get(id=1)
		score.is_visible = False
		score.save()

		expected_visibility = Score.objects.get(id=1).is_visible

		self.assertEqual(expected_visibility, False)