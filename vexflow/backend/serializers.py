from rest_framework import serializers
from backend.models import *
from django.contrib.auth.models import User

class ModeSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Mode
		fields = '__all__'


class ChordSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Chord
		fields = '__all__'


class ProgressionSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Progression
		fields = '__all__'


class ScoreSerializer(serializers.ModelSerializer):
	
	class Meta:
		model = Score
		fields = '__all__'