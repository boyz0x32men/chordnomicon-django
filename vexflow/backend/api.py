from backend.models import *
from backend.serializers import *
from rest_framework import viewsets, permissions

class ModeViewSet(viewsets.ModelViewSet):
    queryset = Mode.objects.all()
    permission_classes = [
        permissions.AllowAny #Change this
    ]
    serializer_class = ModeSerializer


class ChordViewSet(viewsets.ModelViewSet):
    queryset = Chord.objects.all()
    permission_classes = [
        permissions.AllowAny #Change this
    ]
    serializer_class = ChordSerializer


class ProgressionViewSet(viewsets.ModelViewSet):
    queryset = Progression.objects.all()
    permission_classes = [
        permissions.AllowAny #Change this
    ]
    serializer_class = ProgressionSerializer


class ScoreViewSet(viewsets.ModelViewSet):
    queryset = Score.objects.all()
    permission_classes = [
        permissions.AllowAny #Change this
    ]
    serializer_class = ScoreSerializer