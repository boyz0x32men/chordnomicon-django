from django.urls import include, path
from django.contrib import admin

urlpatterns = [
    path('', include('backend.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('rest-auth/', include('rest_auth.urls')),
    path('rest-auth/registration/', include('rest_auth.registration.urls'))
]
